#pragma once
#include"player_obj.h"

class Player
{
public:
    Player(std::shared_ptr<ModelResource>resource);
    void Update(float elapsedTime);
    void Move(float elapsedTime);
    void BubbleSet();
    void ASet(bool flag);
    const VECTOR3F GetPosition() { return pos; }
    const VECTOR3F GetAngle() { return angle; }
    const float GetO2() { return O2; }
private:
    std::shared_ptr<PlayerObj> PL;
    VECTOR3F pos;
    VECTOR3F angle;
    float O2;
    bool flag;
    int a = 0;
    int b = 0;
};