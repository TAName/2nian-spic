#pragma once
#include"button_obj.h"
#include"stage.h"

class StageButton:public Stage
{
public:
	StageButton(std::shared_ptr<StaticMesh>mesh01, std::shared_ptr<StaticMesh>mesh02, int meshNo,int gateNo);
	void Update(float elapasd_time);
	
private:
	std::shared_ptr<ButtonObj>buttonObj;
};