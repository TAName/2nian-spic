#pragma once
#include"geometric_primitive.h"
#include"obj3d.h"
#include<memory>

class SupplyAreaObj:public Obj3D
{
public:
	SupplyAreaObj(std::shared_ptr<GeometricPrimitive>prime);
	//setter
	void SetSupplyFlag(const bool flag) { supplyFlag = flag; }
	void SetLength(const float len) { length = len; }
	void SetHitLength(const float len) { hitLength = len; }
	//getter
	const float GetLength() { return length; }
	const float GetHitLength() { return hitLength; }
	const bool GetSupplyFlag() { return supplyFlag; }
	std::shared_ptr<GeometricPrimitive>GetPrimitive() { return primitive; }
private:
	float length;
	bool supplyFlag;
	float hitLength;
	std::shared_ptr<GeometricPrimitive>primitive;
};