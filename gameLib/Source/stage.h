#pragma once
#include"stage_obj.h"

class Stage
{
public:
	Stage(std::shared_ptr<StaticMesh>mesh, const int meshNo);
	virtual void Update(float elapasd_time);
	std::shared_ptr<StageObj>GetStageObj() { return obj; }
private:
	std::shared_ptr<StageObj>obj;
};