#include "stage_button.h"
#include"obj_manager.h"
#include"sound_manager.h"

StageButton::StageButton(std::shared_ptr<StaticMesh> mesh01, std::shared_ptr<StaticMesh> mesh02, int meshNo, int gateNo):Stage(mesh01,meshNo)
{
	if(gateNo>=0)buttonObj = std::make_shared<ButtonObj>(mesh02, gateNo);
	else buttonObj = std::make_shared<ButtonObj>(mesh02);
	pObjManager->SetButtons(buttonObj);
}

void StageButton::Update(float elapasd_time)
{
	GetStageObj()->CalculateTransform();
	FLOAT4X4 world = GetStageObj()->GetWorld();
	VECTOR3F position = GetStageObj()->GetPosition();
	VECTOR3F vec = VECTOR3F(world._21, world._22, world._23);
	VECTOR3F s = GetStageObj()->GetScale();
	float time = buttonObj->GetPushTime();

	VECTOR3F hitArea = GetStageObj()->GetHitArea();
	VECTOR3F front = VECTOR3F(world._31, world._32, world._33);
	VECTOR3F up = VECTOR3F(world._21, world._22, world._23);
	VECTOR3F right = VECTOR3F(world._11, world._12, world._13);
	VECTOR3F hitArea2 = VECTOR3F(hitArea.x * 0.4f, hitArea.y, hitArea.z * 0.4f);
	VECTOR3F area = front * hitArea.z + up * hitArea.y + right * hitArea.x;
	//area *= GetStageObj()->GetScale().x;
	VECTOR3F min, max;
	if (area.x >= -area.x)
	{
		max.x = area.x;
		min.x = -area.x;
	}
	else
	{
		max.x = -area.x;
		min.x = area.x;
	}
	if (area.y >= 0.f)
	{
		max.y = area.y;
		min.y = 0.f;
	}
	else
	{
		max.y = 0.f;
		min.y = area.y;
	}

	if (area.z >= -area.z)
	{
		max.z = area.z;
		min.z = -area.z;
	}
	else
	{
		max.z = -area.z;
		min.z = area.z;
	}
	GetStageObj()->SetMin(min);
	GetStageObj()->SetMax(max);

	area = front * hitArea2.z + up * hitArea2.y + right * hitArea2.x;
	//area *= GetStageObj()->GetScale().x;
	if (area.x >= -area.x)
	{
		max.x = area.x;
		min.x = -area.x;
	}
	else
	{
		max.x = -area.x;
		min.x = area.x;
	}
	if (area.y >= 0)
	{
		max.y = area.y;
		min.y = 0;
	}
	else
	{
		max.y = 0;
		min.y = area.y;
	}

	if (area.z >= -area.z)
	{
		max.z = area.z;
		min.z = -area.z;
	}
	else
	{
		max.z = -area.z;
		min.z = area.z;
	}
	buttonObj->SetMin(min);
	buttonObj->SetMax(max);


	if (!buttonObj->GetPushFlag())
	{
		position += VECTOR3F(vec.x, vec.y, vec.z);
		buttonObj->SetPosition(VECTOR3F(position));
		buttonObj->SetScale(s);
		buttonObj->SetAngle(GetStageObj()->GetAngle());
		buttonObj->CalculateTransform();
		if (time > .0f)time -= elapasd_time;
		else time = .0f;
		buttonObj->SetPushTime(time);
	}
	else
	{
		position += VECTOR3F(vec.x, vec.y, vec.z)*(1.0f-time)*0.5f;
		buttonObj->SetPosition(VECTOR3F(position));
		buttonObj->SetScale(s);
		buttonObj->SetAngle(GetStageObj()->GetAngle());
		buttonObj->CalculateTransform();

		if (time < 1.0f)time += elapasd_time;
		else
		{
			time = 1.0f;
			for (auto& gate : pObjManager->GetGateObj())
			{
				if (gate->GetOpenFlag())continue;
				if (gate->GetGateNo() == buttonObj->GetGateNo())
				{
					gate->SetOpenFlag(true);
					pSoundManager.Play(SoundManager::SOUND::Button);
					pSoundManager.SetVolume(SoundManager::SOUND::Button, 3.0f);

				}
			}
		}
		buttonObj->SetPushTime(time);

	}
	buttonObj->SetExist(GetStageObj()->GetExist());
}
