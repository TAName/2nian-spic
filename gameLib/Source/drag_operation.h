#pragma once
#include"static_obj.h"
#include"stage_obj.h"

class DragOperation
{
public:
	DragOperation(ID3D11Device*device);
	//void SetDragObj(std::shared_ptr<Obj3D>obj) {}
	void SetDragObj(std::shared_ptr<StaticObj>obj);
	//void SetDragObj(Obj3D*obj){}
	bool Editor(const VECTOR3F& start, const VECTOR3F& end);
	void SetOperationFlag(const bool flag) { operationFlag = flag; }

private:
	std::shared_ptr<StaticObj>dragObj;
	std::shared_ptr<StaticObj>arrow[3];
	bool dragFlag;
	bool operationFlag;
	int hitDragArrow;
	int moveParamerter;
	VECTOR3F defaultPosition;
	VECTOR3F defaultOutPosition;
	VECTOR3F beforeMousePosition;
	VECTOR3F loacalPosition[3];
	void Move(const VECTOR3F& start, const VECTOR3F& end);

};