#include "bubble_obj.h"

BubbleObj::BubbleObj(std::shared_ptr<GeometricPrimitive> geo)
{
    Sphere = geo;
}

BubbleObj::BubbleObj(ID3D11Device* device):Obj3D()
{
    Sphere = std::make_shared<GeometricSphere>(device,32,16);
}

void BubbleObj::Update()
{
    CalculateTransform();
}
