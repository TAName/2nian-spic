#include "character.h"

Character::Character(std::shared_ptr<ModelResource> resource):Obj3D()
{
	model = std::make_unique<Model>(resource);
}

void Character::CalculateMultipleTransform()
{
	//ワールド行列
	CalculateTransform();
	//モデルのローカル行列
	model->CalculateLocalTransform();
	//モデルのワールド行列
	model->CalculateWorldTransform(DirectX::XMLoadFloat4x4(&GetWorld()));
}

