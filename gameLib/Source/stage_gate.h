#pragma once
#include"stage.h"
#include"gate_obj.h"
class StageGate :public Stage
{
public:
	StageGate(std::shared_ptr<StaticMesh>mesh01, std::shared_ptr<StaticMesh>mesh02, int meshNo,int gateNo);
	void Update(float elapasd_time);
private:
	std::shared_ptr<GateObj>mGateObj;
};