#pragma once
#include"obj3d.h"
#include"model.h"
#include"model_resource.h"
#include<memory>

class Character :public Obj3D
{
public:
	Character(std::shared_ptr<ModelResource>resource);
	//複数の行列の計算関数
	void CalculateMultipleTransform();

	//getter
	Model* GetModel() { return model.get(); }
private:
	std::unique_ptr<Model>model;
	
};