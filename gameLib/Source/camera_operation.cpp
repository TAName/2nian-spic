#include "camera_operation.h"
#include<Windows.h>
#include"key_board.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

CameraOperation::CameraOperation(std::shared_ptr<Camera> camera):mType(CAMERA_TYPE::NORMAL)
{
	mCamera = camera;
	VECTOR3F focusF = mCamera->GetFocus();
	VECTOR3F eyeF = mCamera->GetEye();
	VECTOR3F l = focusF - eyeF;
	distance = sqrtf(l.x * l.x + l.y * l.y + l.z * l.z);
}

void CameraOperation::Update(float elapsedTime)
{
	switch (mType)
	{
	case CAMERA_TYPE::DEBUG:
		DebugCamera();
		break;
	}
}

void CameraOperation::DebugCamera()
{
	VECTOR3F focusF = mCamera->GetFocus();
	VECTOR3F upF = mCamera->GetUp();
	VECTOR3F rightF = mCamera->GetRight();
	VECTOR3F eyeF = mCamera->GetEye();
	POINT cursor;
	::GetCursorPos(&cursor);

	oldCursor = newCursor;
	newCursor = VECTOR2F(static_cast<float>(cursor.x), static_cast<float>(cursor.y));

	float move_x = (newCursor.x - oldCursor.x) * 0.02f;
	float move_y = (newCursor.y - oldCursor.y) * 0.02f;

	// AltL[
	if (pKeyBoad.PressedState(VK_MENU))
	{
		if (pKeyBoad.PressedState(VK_LBUTTON))
		{
			// Y²ñ]
			rotate.y += move_x * 0.5f;
			if (rotate.y > DirectX::XM_PI)
			{
				rotate.y -= DirectX::XM_2PI;
			}
			else if (rotate.y < -DirectX::XM_PI)
			{
				rotate.y += DirectX::XM_2PI;
			}
			// X²ñ]
			rotate.x += move_y * 0.5f;
			if (rotate.x > DirectX::XM_PI)
			{
				rotate.x -= DirectX::XM_2PI;
			}
			else if (rotate.x < -DirectX::XM_PI)
			{
				rotate.x += DirectX::XM_2PI;
			}
		}
		else if (pKeyBoad.PressedState(VK_MBUTTON))
		{
			// ½sÚ®
			float s = distance * 0.035f;
			float x = -move_x * s;
			float y = move_y * s;

			focusF.x += rightF.x * x;
			focusF.y += rightF.y * x;
			focusF.z += rightF.z * x;

			focusF.x += upF.x * y;
			focusF.y += upF.y * y;
			focusF.z += upF.z * y;
		}
		else if (pKeyBoad.PressedState(VK_RBUTTON))
		{
			// Y[
			distance += (-move_y - move_x) * distance * 0.1f;
		}


		float sx = ::sinf(rotate.x);
		float cx = ::cosf(rotate.x);
		float sy = ::sinf(rotate.y);
		float cy = ::cosf(rotate.y);

		DirectX::XMVECTOR front = DirectX::XMVectorSet(-cx * sy, -sx, -cx * cy, 0.0f);
		DirectX::XMVECTOR right = DirectX::XMVectorSet(cy, 0, -sy, 0.0f);
		DirectX::XMVECTOR up = DirectX::XMVector3Cross(right, front);

		DirectX::XMVECTOR focus = DirectX::XMLoadFloat3(&focusF);
		DirectX::XMVECTOR distance = DirectX::XMVectorSet(this->distance, this->distance, this->distance, 0.0f);
		DirectX::XMVECTOR eye = DirectX::XMVectorSubtract(focus, DirectX::XMVectorMultiply(front, distance));
		DirectX::XMStoreFloat3(&eyeF, eye);
		DirectX::XMStoreFloat3(&upF, up);
		mCamera->SetEye(eyeF);
		mCamera->SetFocus(focusF);
		mCamera->SetUp(upF);
	}
#ifdef USE_IMGUI
	ImGui::Begin("DebugCamera");
	ImGui::Text("[eye] x:%f y:%f z:%f", eyeF.x, eyeF.y, eyeF.z);
	ImGui::Text("[focus] x:%f y:%f z:%f", focusF.x, focusF.y, focusF.z);
	ImGui::Text("[up] x:%f y:%f z:%f", upF.x, upF.y, upF.z);
	ImGui::Text("[front] x:%f y:%f z:%f", mCamera->GetFront().x, mCamera->GetFront().y, mCamera->GetFront().z);
	ImGui::End();
#endif
}
