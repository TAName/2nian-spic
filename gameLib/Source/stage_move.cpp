#include "stage_move.h"
#include"obj_manager.h"
#include"stage_manager.h"


StageMove::StageMove(std::shared_ptr<StaticMesh> mesh, const int meshNo)
{
	obj = std::make_shared<MoveObj>(mesh, meshNo);
	pStageManager->SetStageObj(obj);
	pObjManager->SetMoves(obj);
	obj->SetScale(VECTOR3F(1, 1, 1));
}

void StageMove::Update(float elapasd_time)
{
	if (obj->GetMoveFlage())
	{
		if (!obj->GetHitButton())
		{
			VECTOR3F position = obj->GetPosition();
			position.y += 0.5f;
			obj->SetPosition(position);
			obj->GetBubble()->SetPosition(VECTOR3F(position.x,position.y+3.f,position.z));
		}
	}
	else
	{
		if (obj->GetBubble() != nullptr)
		{
			
			obj->ResetBubble();
		}
	}
	obj->CalculateTransform();
	FLOAT4X4 world = obj->GetWorld();
	VECTOR3F hitArea = obj->GetHitArea();
	VECTOR3F front = VECTOR3F(world._31, world._32, world._33);
	VECTOR3F up = VECTOR3F(world._21, world._22, world._23);
	VECTOR3F right = VECTOR3F(world._11, world._12, world._13);

	VECTOR3F area = front * hitArea.z + up * hitArea.y + right * hitArea.x;
	//area *= obj->GetScale().x;
	VECTOR3F min, max;
	if (area.x >= -area.x)
	{
		max.x = area.x;
		min.x = -area.x;
	}
	else
	{
		max.x = -area.x;
		min.x = area.x;
	}
	if (area.y >= 0.f)
	{
		max.y = area.y;
		min.y = 0.f;
	}
	else
	{
		max.y = 0.f;
		min.y = area.y;
	}

	if (area.z >= -area.z)
	{
		max.z = area.z;
		min.z = -area.z;
	}
	else
	{
		max.z = -area.z;
		min.z = area.z;
	}
	obj->SetMin(min);
	obj->SetMax(max);

}
