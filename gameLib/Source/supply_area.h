#pragma once
#include"supply_area_obj.h"
#include"stage_obj.h"

class SupplyArea
{
public:
	SupplyArea(std::shared_ptr<StaticMesh>mesh ,std::shared_ptr<GeometricPrimitive>primitive,const int meshNo);
	void Update(float elapasd_time);
	std::shared_ptr<SupplyAreaObj>GetSupplyObj() { return areaObj; }
private:
	std::shared_ptr<SupplyAreaObj>areaObj;
	std::shared_ptr<StageObj>dragObj;
};