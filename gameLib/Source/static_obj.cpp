#include "static_obj.h"

StaticObj::StaticObj(std::shared_ptr<StaticMesh> mesh):mColor(1,1,1,1)
{
	mMesh = mesh;
	exist = true;
	mHitArea = VECTOR3F(0, 0, 0);
}

int StaticObj::RayPick(const VECTOR3F& startPosition, const VECTOR3F& endPosition, VECTOR3F* outPosition, VECTOR3F* outNormal)
{
	DirectX::XMMATRIX W = DirectX::XMLoadFloat4x4(&GetWorld());
	DirectX::XMMATRIX invers_W;
	invers_W = DirectX::XMMatrixInverse(NULL, W);
	//オブジェクト空間でのレイに変換
	DirectX::XMVECTOR startposition = DirectX::XMLoadFloat3(&startPosition);
	DirectX::XMVECTOR endposition = DirectX::XMLoadFloat3(&endPosition);
	DirectX::XMVECTOR loacal_start = DirectX::XMVector3TransformCoord(startposition, invers_W);
	DirectX::XMVECTOR loacal_end = DirectX::XMVector3TransformCoord(endposition, invers_W);
	//レイピック
	float outDistance;
	VECTOR3F start, end;
	DirectX::XMStoreFloat3(&start, loacal_start);
	DirectX::XMStoreFloat3(&end, loacal_end);
	int ret = mMesh->RayPick(start, end, outPosition, outNormal, &outDistance);
	if (ret != -1)
	{
		//オブジェクト空間からワールド空間へ変換
		DirectX::XMVECTOR out_p = DirectX::XMLoadFloat3(outPosition);
		DirectX::XMVECTOR out_n = DirectX::XMLoadFloat3(outNormal);

		DirectX::XMVECTOR world_position = DirectX::XMVector3TransformCoord(out_p, W);
		DirectX::XMVECTOR world_normal = DirectX::XMVector3TransformNormal(out_n, W);

		DirectX::XMStoreFloat3(outPosition, world_position);
		DirectX::XMStoreFloat3(outNormal, world_normal);
	}
	return ret;
}

int StaticObj::RayPick(const VECTOR3F& startPosition, const VECTOR3F& endPosition, VECTOR3F* outPosition, VECTOR3F* outNormal, float* outDistance)
{
	DirectX::XMMATRIX W = DirectX::XMLoadFloat4x4(&GetWorld());
	DirectX::XMMATRIX invers_W;
	invers_W = DirectX::XMMatrixInverse(NULL, W);
	//オブジェクト空間でのレイに変換
	DirectX::XMVECTOR startposition = DirectX::XMLoadFloat3(&startPosition);
	DirectX::XMVECTOR endposition = DirectX::XMLoadFloat3(&endPosition);
	DirectX::XMVECTOR loacal_start = DirectX::XMVector3TransformCoord(startposition, invers_W);
	DirectX::XMVECTOR loacal_end = DirectX::XMVector3TransformCoord(endposition, invers_W);
	//レイピック
	VECTOR3F start, end;
	DirectX::XMStoreFloat3(&start, loacal_start);
	DirectX::XMStoreFloat3(&end, loacal_end);
	int ret = mMesh->RayPick(start, end, outPosition, outNormal, outDistance);
	if (ret != -1)
	{
		//オブジェクト空間からワールド空間へ変換
		DirectX::XMVECTOR out_p = DirectX::XMLoadFloat3(outPosition);
		DirectX::XMVECTOR out_n = DirectX::XMLoadFloat3(outNormal);

		DirectX::XMVECTOR world_position = DirectX::XMVector3TransformCoord(out_p, W);
		DirectX::XMVECTOR world_normal = DirectX::XMVector3TransformNormal(out_n, W);

		DirectX::XMStoreFloat3(outPosition, world_position);
		DirectX::XMStoreFloat3(outNormal, world_normal);
	}
	return ret;
}
