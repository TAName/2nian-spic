#include "scene_clear.h"
#include"scene_manager.h"
#include"key_board.h"
#include"blend_state.h"
#ifdef USE_IMGUI
#include"imgui.h"
#endif
#include"sound_manager.h"

std::unique_ptr<blend_state> blend_clear;
SceneClear::SceneClear(ID3D11Device* device)
{
	Rank_S	= std::make_unique<Sprite>(device, L"Data/image/Rank_S.png");
	Rank_A = std::make_unique<Sprite>(device, L"Data/image/Rank_A.png");
	Rank_B = std::make_unique<Sprite>(device, L"Data/image/Rank_B.png");
	Rank_C = std::make_unique<Sprite>(device, L"Data/image/Rank_C.png");

	gameclear = std::make_unique<Sprite>(device, L"Data/image/game_clear.png");
	clear_time = std::make_unique<Sprite>(device, L"Data/image/font.png");
	stage_select_title01 = std::make_unique<Sprite>(device, L"Data/image/stage_select_title_font01.png");
	stage_select_title02 = std::make_unique<Sprite>(device, L"Data/image/stage_select_title_font02.png");
	blend_clear = std::make_unique<blend_state>(device, BLEND_MODE::ALPHA);
	titleorselect = 0;
	pSoundManager.Play(SoundManager::SOUND::BGM_CLEAR, true);
	pSoundManager.SetVolume(SoundManager::SOUND::BGM_CLEAR, 1.0f);

}

void SceneClear::Update(float elapsed_time)
{
#ifdef USE_IMGUI
	ImGui::Begin("chack");
	ImGui::Text("data3 %d:data2 %d:data1 %d:data0 %d", data[3], data[2], data[1],data[0]);
	ImGui::End();
#endif
	/*if (pKeyBoad.RisingState(KeyLabel::SPACE))
	{
		pSceneManager.ChangeScene(SCENETYPE::TITLE);
		return;
	}*/
	if (titleorselect == 0 && pKeyBoad.RisingState(KeyLabel::SPACE))
	{
		pSoundManager.Stop(SoundManager::SOUND::BGM_CLEAR);

		pSoundManager.Play(SoundManager::SOUND::Decision);
		pSoundManager.SetVolume(SoundManager::SOUND::Decision, 1.0f);

		pSceneManager.ChangeScene(SCENETYPE::GAME);
		return;
	}
	if (titleorselect == 1 && pKeyBoad.RisingState(KeyLabel::SPACE))
	{
		pSoundManager.Stop(SoundManager::SOUND::BGM_CLEAR);

		pSoundManager.Play(SoundManager::SOUND::Decision);
		pSoundManager.SetVolume(SoundManager::SOUND::Decision, 1.0f);

		pSceneManager.ChangeScene(SCENETYPE::TITLE);
		return;
	}
	//矢印
	if (titleorselect == 0 && pKeyBoad.RisingState(KeyLabel::RIGHT))
	{
		titleorselect = 1;
	}
	if (titleorselect == 1 && pKeyBoad.RisingState(KeyLabel::LEFT))
	{
		titleorselect = 0;
	}

	//ランク
	if (data[3] == 0 && data[2] <= 64&& data[1]<192) { rank = 0; }//S
	else if ((data[3] == 0 && data[2] == 192 && data[1] == 0 && data[0] == 0) || (data[3] == 0 && data[2] < 192)) { rank = 1; }//A
	else if((data[3] == 0 && data[2] == 320 && data[1] == 0 && data[0] == 0) || (data[3] == 0 && data[2] < 320)) { rank = 2; }//A
	else { rank = 3; }
}

void SceneClear::Render(ID3D11DeviceContext* context, float elapsed_time)
{
	blend_clear->activate(context);
	gameclear->Render(context, VECTOR2F(0, 0), VECTOR2F(1920,1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
	if (titleorselect == 0)stage_select_title01->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
	if (titleorselect == 1)stage_select_title02->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
	clear_time->Render(context, VECTOR2F(320, 900), VECTOR2F(64, 64), VECTOR2F(data[0], 0), VECTOR2F(64, 64), 0);//1桁目//秒
	clear_time->Render(context, VECTOR2F(256, 900), VECTOR2F(64, 64), VECTOR2F(data[1], 0), VECTOR2F(64, 64), 0);//2桁目//秒
	clear_time->Render(context, VECTOR2F(192, 900), VECTOR2F(64, 64), VECTOR2F(640, 0), VECTOR2F(64, 64), 0);//コロン
	clear_time->Render(context, VECTOR2F(128, 900), VECTOR2F(64, 64), VECTOR2F(data[2], 0), VECTOR2F(64, 64), 0);//1桁目//分
	clear_time->Render(context, VECTOR2F(64, 900), VECTOR2F(64, 64), VECTOR2F(data[3], 0), VECTOR2F(64, 64), 0);//2桁目//分
	
	if (rank == 0)Rank_S->Render(context, VECTOR2F(0, 0), VECTOR2F(1920,1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);//ランクS
	if (rank == 1)Rank_A->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);//ランクA
	if (rank == 2)Rank_B->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);//ランクB
	if (rank == 3)Rank_C->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);//ランクB
	blend_clear->deactivate(context);
}

SceneClear::~SceneClear()
{
}
