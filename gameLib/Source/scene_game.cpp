#include "scene_game.h"
#include"key_board.h"
#include"scene_manager.h"
#include"blend_state.h"
#include"sound_manager.h"
#include"camera_manager.h"
#include"gamepad.h"
#include"obj_manager.h"
#include"stage_manager.h"
#include"bubble_manager.h"
#include"fado_out.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

std::unique_ptr<blend_state> blend[2];
SceneGame::SceneGame(ID3D11Device* device)
{
	loading_thread = std::make_unique<std::thread>([&](ID3D11Device* device)
	{
		std::lock_guard<std::mutex> lock(loading_mutex);

		//mesh = std::make_unique<StaticMesh>(device, "Data/FBX/source/Dragon_ver2.fbx");
		//meshRender = std::make_unique<MeshRender>(device);
		//testStage = std::make_shared<StageObj>(std::move(mesh));
		//testStage->SetPosition(VECTOR3F(0, 0, 0));
		//testStage->SetAngle(VECTOR3F(0, 0, 0));
		//testStage->SetScale(VECTOR3F(1, 1, 1));
		//std::unique_ptr<StaticMesh>mesh2 = std::make_unique<StaticMesh>(device, "Data/FBX/stage01.fbx");
		//testStage2 = std::make_unique<StageObj>(std::move(mesh2));
		//testStage2->SetPosition(VECTOR3F(0, 0, 0));
		//testStage2->SetAngle(VECTOR3F(0, 0, 0));
		//testStage2->SetScale(VECTOR3F(1, 1, 1));
		pObjManager->Create(device);
		//pObjManager->SetStages(testStage);
		//pObjManager->SetStages(testStage2);
		pStageManager->Create(device);
		std::unique_ptr<ModelData>playerData = std::make_unique<ModelData>("Data/FBX/Player_anim/Player_anim.fbx",false);
		std::shared_ptr<ModelResource>playerResource = std::make_shared<ModelResource>(device, std::move(playerData));
		player = std::make_unique<Player>(playerResource);
		pBubbleManager->Create(device);
	}, device);
	description = std::make_unique<Sprite>(device, L"Data/image/controls.png");
	decision = std::make_unique<Sprite>(device, L"Data/image/title_pushspace.png");
	nowLoading = std::make_unique<Sprite>(device, L"Data/image/now.png");
	siro = std::make_unique<Sprite>(device, L"Data/image/siro.png");
	blend[0] = std::make_unique<blend_state>(device, BLEND_MODE::ALPHA);
	//mCamera = std::make_shared<Camera>();
	//mCameraOperation = std::make_unique<CameraOperation>(mCamera);
	//mCameraOperation->SetCameraType(CameraOperation::CAMERA_TYPE::NORMAL);
	pCamera.CreateCamera();
	UI::GetInctance().Init(device);
	endLoading = false;
	timer = 0.f;
}

void SceneGame::Update(float elapsed_time)
{
	if (NowLoadingUpdate(IsNowLoading(),elapsed_time))
	{
		return;
	}
	if (pFadoOut.Update(elapsed_time))
	{
		return;
	}

	if (!pStageManager->Select(elapsed_time)||pFadoOut.GetFado())
	{
		return;
	}
	//mCameraOperation->Update(elapsed_time);
	//mCamera->CalculateMatrix();
	pCamera.Update(elapsed_time);
	//testStage->CalculateTransform();
	//testStage2->CalculateTransform();
	//ImGui::ShowDemoWindow();
	if (pStageManager->Editor())
	{
		player->Update(elapsed_time);
		VECTOR3F position = player->GetPosition();
		pCamera.GetCamera()->SetEye(VECTOR3F(0 + position.x, 30 + position.y, -100));
		pCamera.GetCamera()->SetFocus(position);
		pCamera.GetCamera()->SetUp(VECTOR3F(0, 1, 0));
		UI::GetInctance().Set_O2(player->GetO2());
	}
	pStageManager->Update(elapsed_time);
	pBubbleManager->Updeta(elapsed_time);
	pObjManager->Judge(elapsed_time);
	UI::GetInctance().Update(elapsed_time);
}

void SceneGame::Render(ID3D11DeviceContext* context, float elapsed_time)
{
	D3D11_VIEWPORT viewport;
	UINT num_viewports = 1;
	context->RSGetViewports(&num_viewports, &viewport);

	if (NowLoadingRender(IsNowLoading(),context,VECTOR2F(viewport.Width, viewport.Height)))
	{

		return;
	}

	blend[0]->activate(context);

	if (!pStageManager->SelectRender(context))
	{

		blend[0]->deactivate(context);

		return;
	}

	blend[0]->deactivate(context);

	//test->Render(context, VECTOR2F(0, 0), VECTOR2F(viewport.Width, viewport.Height), VECTOR2F(0, 0), VECTOR2F(1024, 576), 0);
	blend[0]->activate(context);
	siro->Render(context, VECTOR2F(0, 0), VECTOR2F(viewport.Width, viewport.Height), VECTOR2F(0, 0), VECTOR2F(1024, 1024), 0, VECTOR4F(0, 1, 1, 0.4f));
	blend[0]->deactivate(context);


	DirectX::XMMATRIX W, s, r, t;
	//スケール行列
	s = DirectX::XMMatrixScaling(1, 1, 1);
	//回転行列
	r = DirectX::XMMatrixRotationRollPitchYaw(0, 0, 0);
	//移動行列
	t = DirectX::XMMatrixTranslation(0, 0, 0);
	//ワールド行列
	W = s * r * t;
	FLOAT4X4 world;
	DirectX::XMStoreFloat4x4(&world, W);
	const VECTOR4F light = VECTOR4F(0, -0.75f, 1, 0);
	FLOAT4X4 view = pCamera.GetCamera()->GetView();
	FLOAT4X4 projection = pCamera.GetCamera()->GetProjection();
	//meshRender->Begin(context, light, view, projection);
	//meshRender->Render(context, mesh.get(), world);
	//meshRender->End(context);
	blend[0]->activate(context);
	pObjManager->Render(context, light, view, projection);
	pBubbleManager->Render(context, light, view, projection);

	blend[0]->deactivate(context);
	
	blend[0]->activate(context);
	UI::GetInctance().Render(context);
	siro->Render(context, VECTOR2F(0, 0), VECTOR2F(viewport.Width, viewport.Height), VECTOR2F(0, 0), VECTOR2F(1024, 1024), 0, VECTOR4F(0, 1, 1, 0.2f));
	pFadoOut.Render(context);

	blend[0]->deactivate(context);

}

SceneGame::~SceneGame()
{
	pCamera.DestroyCamera();
	pObjManager->Destroy();
	pStageManager->Destroy();
	pBubbleManager->Destroy();
}

bool SceneGame::NowLoadingUpdate(bool isNowLoading, float elapasd_time)
{
	if (!isNowLoading)
	{
		EndLoading();

		if(endLoading)return false;

		if (pKeyBoad.RisingState(KeyLabel::SPACE))
		{
			pSoundManager.Play(SoundManager::SOUND::Decision);
			pSoundManager.SetVolume(SoundManager::SOUND::Decision, 1.0f);
			pSoundManager.Play(SoundManager::SOUND::BGM_Select, true);
			pSoundManager.SetVolume(SoundManager::SOUND::BGM_Select, 1.0f);

			endLoading = true;
		}
	}
	if (timer < 3.0f)
	{
		timer += elapasd_time;
	}
	if(timer>=3.0f)
	{
		timer = 0.f;
	}
	return true;
}

bool SceneGame::NowLoadingRender(bool isNowLoading, ID3D11DeviceContext* context, const VECTOR2F& viewport)
{
	if (!isNowLoading)
	{
		if (endLoading)return false;
		int time = static_cast<int>(timer);
		if (timer-time>0.5f)
		{
			decision->Render(context, VECTOR2F(viewport.x, viewport.y) * 0.7f + VECTOR2F(0, 100), VECTOR2F(500, 200), VECTOR2F(700, 700), VECTOR2F(500, 200), 0);
		}
	}
	else
	{
		VECTOR4F loadingColor = VECTOR4F(1, 1, 1, 1);
		if (timer < 1.0f)
		{
			loadingColor.w = timer;
		}
		else if (timer >= 2.0f&&timer<3.0f)
		{
			loadingColor.w = 3.0f - timer;
		}
		blend[0]->activate(context);
		nowLoading->Render(context, VECTOR2F(viewport.x, viewport.y)*0.8f, VECTOR2F(430, 100) *0.7f, VECTOR2F(0, 0), VECTOR2F(430, 100), 0, loadingColor);
		blend[0]->deactivate(context);
	}
	blend[0]->activate(context);
	description->Render(context, VECTOR2F(viewport.x, viewport.y) * 0.2f, VECTOR2F(viewport.x, viewport.y) * 0.6f, VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
	blend[0]->deactivate(context);

	return true;
}
