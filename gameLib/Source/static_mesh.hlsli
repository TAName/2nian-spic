struct VS_OUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float2 texcoord : TEXCOORD;
};

cbuffer CbScene:register(b0)
{
	float4 lightDirection;
	row_major float4x4 view;
	row_major float4x4 projection;
};

cbuffer CbObj:register(b1)
{
	row_major float4x4 world;
	row_major float4x4 wvp;
	float4 materialColor;

};