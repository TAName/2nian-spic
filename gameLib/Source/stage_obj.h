#pragma once
#include"static_obj.h"
class StageObj:public StaticObj
{
public:
	StageObj(std::shared_ptr<StaticMesh>mesh,int meshNo);
	const int& GetMeshNo() { return mMeshNo; }
private:
	int mMeshNo;
};
