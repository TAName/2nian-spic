#pragma once
#include"scene.h"
#include <thread>
#include <mutex>
#include<memory>
#include"sprite.h"
#include"UI.h"
class SceneClear :public Scene
{
public:
	SceneClear(ID3D11Device* device);
	void Update(float elapsed_time);
	void Render(ID3D11DeviceContext* context, float elapsed_time);
	~SceneClear();
private:
	//Now Loading
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;


	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}
	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}
private:
	int titleorselect;
	int rank;//0:s/1:a/2:b/3:c
	std::array<int, 4>data = UI::GetInctance().get_time();
	std::unique_ptr<Sprite>Rank_S;
	std::unique_ptr<Sprite>Rank_A;
	std::unique_ptr<Sprite>Rank_B;
	std::unique_ptr<Sprite>Rank_C;
	std::unique_ptr<Sprite>clear_time;
	std::unique_ptr<Sprite>gameclear;
	std::unique_ptr<Sprite>stage_select_title01;
	std::unique_ptr<Sprite>stage_select_title02;
};