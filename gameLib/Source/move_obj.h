#pragma once
#include"stage.h"
#include"bubble_obj.h"

class MoveObj :public StageObj
{
public:
	MoveObj(std::shared_ptr<StaticMesh>mesh, int meshNo);
	//setter
	void SetMoveFlag(const bool flag) { moveFlag = flag; }
	void SetHitButton(const bool flag) { hitButton = flag; }
	void SetBubble(std::shared_ptr<BubbleObj>obj) { bubble = obj; }
	//getter
	const bool GetMoveFlage() { return moveFlag; }
	const bool GetHitButton() { return hitButton; }
	std::shared_ptr<BubbleObj>GetBubble() { return bubble; }

	void ResetBubble() { bubble.reset(); }
private:
	bool moveFlag;
	bool hitButton;
	std::shared_ptr<BubbleObj>bubble;
};