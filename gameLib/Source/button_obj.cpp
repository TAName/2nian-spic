#include "button_obj.h"

ButtonObj::ButtonObj(std::shared_ptr<StaticMesh> mesh, int gateNo):StaticObj(mesh)
{
	mGateNo = gateNo;
	mPushFlag = false;
	pushTime = 0.f;
}
