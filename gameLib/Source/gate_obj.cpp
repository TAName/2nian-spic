#include "gate_obj.h"

GateObj::GateObj(std::shared_ptr<StaticMesh>mesh, int gateNo):StaticObj(mesh)
{
	mGateNo = gateNo;
	openFlag = false;
	openTime = 0.0f;
}
