#include "stage_gate.h"
#include"obj_manager.h"

StageGate::StageGate(std::shared_ptr<StaticMesh> mesh01, std::shared_ptr<StaticMesh> mesh02, int meshNo, int gateNo) :Stage(mesh01, meshNo)
{
	mGateObj = std::make_shared<GateObj>(mesh02, gateNo);
	pObjManager->SetGates(mGateObj);
}

void StageGate::Update(float elapasd_time)
{
	GetStageObj()->CalculateTransform();
	FLOAT4X4 world = GetStageObj()->GetWorld();
	VECTOR3F hitArea = GetStageObj()->GetHitArea();
	VECTOR3F front = VECTOR3F(world._31, world._32, world._33);
	VECTOR3F up = VECTOR3F(world._21, world._22, world._23);
	VECTOR3F right = VECTOR3F(world._11, world._12, world._13);

	VECTOR3F area = front * hitArea.z + up * hitArea.y + right * hitArea.x;
	//area *= GetStageObj()->GetScale().x*0.5f;
	VECTOR3F min, max;
	if (area.x >= -area.x)
	{
		max.x = area.x;
		min.x = -area.x;
	}
	else
	{
		max.x = -area.x;
		min.x = area.x;
	}
	if (area.y >= 0.f)
	{
		max.y = area.y;
		min.y = 0.f;
	}
	else
	{
		max.y = 0.f;
		min.y = area.y;
	}

	if (area.z >= -area.z)
	{
		max.z = area.z;
		min.z = -area.z;
	}
	else
	{
		max.z = -area.z;
		min.z = area.z;
	}
	mGateObj->SetMin(min);
	mGateObj->SetMax(max);
	mGateObj->SetHitArea(hitArea);
	mGateObj->SetFramePosition(GetStageObj()->GetPosition() + front * 1.25f);
	float time = mGateObj->GetOpenTime();
	if (!mGateObj->GetOpenFlag())
	{
		mGateObj->SetPosition(GetStageObj()->GetPosition());
		mGateObj->SetScale(GetStageObj()->GetScale());
		mGateObj->SetAngle(GetStageObj()->GetAngle());
		mGateObj->CalculateTransform();

	}
	else
	{
		VECTOR3F velocity = VECTOR3F(0.f, 20.f, 0.f);
		velocity *= time;
		if (time < 1.0f)time += elapasd_time;
		mGateObj->SetOpenTime(time);
		mGateObj->SetPosition(GetStageObj()->GetPosition() + velocity);
		mGateObj->SetScale(GetStageObj()->GetScale());
		mGateObj->SetAngle(GetStageObj()->GetAngle());
		mGateObj->CalculateTransform();


	}
	mGateObj->SetExist(GetStageObj()->GetExist());

}
