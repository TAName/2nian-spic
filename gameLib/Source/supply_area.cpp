#include "supply_area.h"
#include"obj_manager.h"
#include"stage_manager.h"
SupplyArea::SupplyArea(std::shared_ptr<StaticMesh> mesh, std::shared_ptr<GeometricPrimitive>primitive, const int meshNo)
{
	areaObj = std::make_shared<SupplyAreaObj>(primitive);
	dragObj = std::make_shared<StageObj>(mesh, meshNo);
	pStageManager->SetStageObj(dragObj);
	pObjManager->SetSupplyss(areaObj);
	//pObjManager->SetStages(dragObj);
}

void SupplyArea::Update(float elapasd_time)
{
	float length = areaObj->GetLength();
	if (areaObj->GetSupplyFlag())
	{
		length -= elapasd_time*0.2f;
		dragObj->SetScale(dragObj->GetScale() * length);
	}
	areaObj->SetSupplyFlag(false);

#ifdef USE_IMGUI
	if (!dragObj->GetExist())
	{
		length = 0.0f;
	}

	dragObj->CalculateTransform();
#endif
	areaObj->SetScale(dragObj->GetScale()*2.f);
	areaObj->SetHitLength(dragObj->GetHitArea().x * areaObj->GetScale().x * length);
	areaObj->SetPosition(dragObj->GetPosition());
	areaObj->SetLength(length);
	areaObj->CalculateTransform();


}
