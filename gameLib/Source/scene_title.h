#pragma once
#include"scene.h"
#include <thread>
#include <mutex>
#include<memory>
#include"sprite.h"
#include"character.h"
#include"model_renderer.h"

class SceneTitle :public Scene
{
public:
	SceneTitle(ID3D11Device* device);
	void Update(float elapsed_time);
	void Render(ID3D11DeviceContext* context, float elapsed_time);
	~SceneTitle();
private:
	//Now Loading
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;


	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}
	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}
private:
	int push_count;
	std::unique_ptr<Sprite>title;
	std::unique_ptr<Sprite>title_push_space;
	std::unique_ptr<Sprite>siro;
	std::unique_ptr<Character>character;
	std::unique_ptr<ModelRenderer>renderer;
};