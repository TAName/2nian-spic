#include "player_obj.h"

PlayerObj::PlayerObj(std::shared_ptr<ModelResource> resouce):Character(resouce)
{
    model = GetModel();
}

void PlayerObj::SetAnim(int animindex)
{
    model->PlayAnimation(animindex, true);
}

void PlayerObj::AnimUpdate(float elapsed_time)
{
    model->UpdateAnimation(elapsed_time);
}
