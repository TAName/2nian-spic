#include "UI.h"


void UI::Init(ID3D11Device* device)
{
	time = std::make_unique<Sprite>(device, L"Data/image/font.png");
	gauge_frame = std::make_unique<Sprite>(device, L"Data/image/gauge_frame.png");
	gauge_bar = std::make_unique<Sprite>(device, L"Data/image/gauge.png");
	for (int i = 0; i < 4; i++) { count[i] = 0; time_x[i] = 0; }
}
void UI::Update(float elapsed_time)
{
	
	count[0] += elapsed_time;//1����//�b
	if (count[0] >= 10) 
	{ 
		count[0] = 0; 
		count[1] += 1;//2����//�b 
		if (count[1] >= 6) 
		{
			count[1] = 0;
			count[2] += 1;//1����//��
			if (count[2] >= 10)
			{
				count[2] = 0;
				count[3] += 1;//2����//��
			}
		}
	}
	time_x[0] = (int)count[0] * 64;//1����//�b
	time_x[1] = (int)count[1] * 64;//2����//�b
	time_x[2] = (int)count[2] * 64;//1����//��
	time_x[3] = (int)count[3] * 64;//2����//��

	//�_�f�Q�[�W
	
	bar_width = (bar / 1000.f) * 384.f;
}



void UI::Render(ID3D11DeviceContext* devicecontext)
{
	time->Render(devicecontext, VECTOR2F(320, 0), VECTOR2F(64, 64), VECTOR2F(time_x[0], 0), VECTOR2F(64, 64), 0);//1����//�b
	time->Render(devicecontext, VECTOR2F(256, 0), VECTOR2F(64, 64), VECTOR2F(time_x[1], 0), VECTOR2F(64, 64), 0);//2����//�b
	time->Render(devicecontext, VECTOR2F(192, 0), VECTOR2F(64, 64), VECTOR2F(640, 0), VECTOR2F(64, 64), 0);//�R����
	time->Render(devicecontext, VECTOR2F(128, 0), VECTOR2F(64, 64), VECTOR2F(time_x[2], 0), VECTOR2F(64, 64), 0);//1����//��
	time->Render(devicecontext, VECTOR2F(64, 0), VECTOR2F(64, 64), VECTOR2F(time_x[3], 0), VECTOR2F(64, 64), 0);//1����//��
	gauge_bar->Render(devicecontext, VECTOR2F(50, 950), VECTOR2F(bar_width, 64), VECTOR2F(0, 0), VECTOR2F(384, 64), 0);
	gauge_frame->Render(devicecontext, VECTOR2F(50, 950), VECTOR2F(384, 64), VECTOR2F(0, 0), VECTOR2F(384, 64), 0);
}

