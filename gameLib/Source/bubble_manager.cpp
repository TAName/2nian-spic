#include "bubble_manager.h"
BubbleManager* BubbleManager::bubblemanager = nullptr;
BubbleManager::BubbleManager(ID3D11Device* device)
{
    PriRender = std::make_unique<PrimitiveRender>(device);
    geome = std::make_shared<GeometricSphere>(device, 32, 16);
}

void BubbleManager::Updeta(float elapsed_time)
{
    for (auto& bubble : bub) {
		bubble->Update(elapsed_time);
    }
	auto remove01 = std::remove_if(bub.begin(), bub.end(), [](std::shared_ptr<Bubble>obj) {if (obj->GetObj()->GetExist())return false;else return true;});
	bub.erase(remove01, bub.end());
	auto remove02 = std::remove_if(bubbles.begin(), bubbles.end(), [](std::shared_ptr<BubbleObj>obj) {if (obj->GetExist())return false;else return true;});
	bubbles.erase(remove02, bubbles.end());

}

void BubbleManager::Render(ID3D11DeviceContext* context, const VECTOR4F& light, const FLOAT4X4& view, const FLOAT4X4& projection)
{
    PriRender->Begin(context, light, view, projection);
    for (auto& bubble : bubbles) {
        PriRender->Render(context, bubble->GetSphere(), bubble->GetWorld(), bubble->GetColor());
    }
    PriRender->End(context);
}
void BubbleManager::Set(VECTOR3F pos, VECTOR3F angle)
{
	bub.back()->Set(pos, angle);
}
void BubbleManager::AA()
{
    bub.push_back(std::make_shared<Bubble>(geome));
}
BubbleManager::~BubbleManager() {
    PriRender.reset();
    bubbles.clear();
}