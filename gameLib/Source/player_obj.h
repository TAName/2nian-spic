#pragma once
#include"character.h"

class PlayerObj :public Character
{
public:
	PlayerObj(std::shared_ptr<ModelResource>resouce);
    void SetAnim(int animindex);
    void AnimUpdate(float elapsed_time);
	const bool GetMoveFlag() { return moveFlag; }
	void SetMoveFlaf(const bool flag) { moveFlag = flag; }
	void SetO2(const float o) { o2 = o; }
	const float GetO2() { return o2; }
private:
	float o2;
	bool moveFlag;
    Model* model;
};