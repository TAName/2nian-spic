#pragma once
#include"sprite.h"
#include<memory>

class FadoOut
{
public:
	bool Update(float elapasd_time);
	void Render(ID3D11DeviceContext* context);
	static FadoOut& GetInctance()
	{
		static FadoOut out;
		return out;
	}
	void Load(ID3D11Device* device);
	void SetNextScene(const int next)
	{
		nextScene = next;
		fadoFlag = true;
		color.w = 0.f;
	}
	bool GetFado() { return fadoFlag; }
	bool GetFadoEnd() { return fadoEndFlag; }
	const int GetNextScene() {
		return nextScene;
	}
private:
	FadoOut():color(0,0,0,0),nextScene(-1), fadoEndFlag(false),fadoFlag(false){}
	std::unique_ptr<Sprite>fado;
	VECTOR4F color;
	int nextScene;
	bool fadoEndFlag;
	bool fadoFlag;
};
#define pFadoOut (FadoOut::GetInctance())