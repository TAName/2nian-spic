#pragma once
#include "bubble_obj.h"

class Bubble {
public:
    Bubble(std::shared_ptr<GeometricPrimitive>geo);
    Bubble(ID3D11Device* device);
    void Update(float elapsed_time);//ステージのブロックに当たった時の処理
    void Set(VECTOR3F pos, VECTOR3F angle);
	std::shared_ptr<BubbleObj>GetObj() { return bubble; }
private:
    std::shared_ptr<BubbleObj> bubble;
	bool end;
};