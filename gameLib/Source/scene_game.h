#pragma once
#include"scene.h"
#include <thread>
#include <mutex>
#include<memory>
#include"sprite.h"
#include"static_mesh.h"
#include"stage_obj.h"
#include"player.h"
#include"UI.h"

class SceneGame :public Scene
{
public:
	SceneGame(ID3D11Device*device);
	void Update(float elapsed_time);
	void Render(ID3D11DeviceContext* context, float elapsed_time);
	~SceneGame();
private:
	//Now Loading
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;


	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}
	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}
	bool NowLoadingUpdate(bool isNowLoading, float elapasd_time);
	bool NowLoadingRender(bool isNowLoading, ID3D11DeviceContext*context,const VECTOR2F&viewport);
private:
	std::unique_ptr<Sprite>description;
	std::unique_ptr<Sprite>decision;
	std::unique_ptr<Sprite>nowLoading;
	std::unique_ptr<Sprite>siro;
	std::unique_ptr<MeshRender>meshRender;
	std::unique_ptr<StaticMesh>mesh;
	std::unique_ptr<Player>player;
	std::shared_ptr<StageObj>testStage;
	std::shared_ptr<StageObj>testStage2;
	bool endLoading = false;
	float timer;
};