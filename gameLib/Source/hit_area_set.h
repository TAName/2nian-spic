#pragma once
#include"static_obj.h"
#include"stage_obj.h"

class HitAreaSet
{
public:
	HitAreaSet(ID3D11Device* device, const int meshNo);
	//void SetDragObj(std::shared_ptr<Obj3D>obj) {}
	void SetDragObj(std::shared_ptr<StaticObj>obj,const int meshNo);
	//void SetDragObj(Obj3D*obj){}
	bool Editor();
	void SetOperationFlag(const bool flag) { operationFlag = flag; }
	void SetArea(const VECTOR3F& area, const int meshNo) { setArea[meshNo] = area; }
	std::vector<VECTOR3F>GetArea() { return setArea; }
private:
	std::shared_ptr<StaticObj>setObj;
	std::shared_ptr<StaticObj>drowObj;
	bool operationFlag;
	std::vector<VECTOR3F>setArea;
	float area[3];
	int mMeshNo;
};