#pragma once

#include"move_obj.h"

class StageMove
{
public:
	StageMove(std::shared_ptr<StaticMesh>mesh, const int meshNo);
	void Update(float elapasd_time);
	std::shared_ptr<MoveObj>GetObj() { return obj; }
private:
	std::shared_ptr<MoveObj>obj;
};