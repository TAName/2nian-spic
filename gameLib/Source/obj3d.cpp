#include "obj3d.h"

Obj3D::Obj3D():mPosition(0,0,0),mScale(0,0,0),mAngle(0,0,0),mWorld(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
{
}

void Obj3D::CalculateTransform()
{
	DirectX::XMMATRIX w, s, r, t;

	//スケール行列
	s = DirectX::XMMatrixScaling(mScale.x, mScale.y, mScale.z);
	//回転行列
	r = DirectX::XMMatrixRotationRollPitchYaw(mAngle.x, mAngle.y, mAngle.z);
	//移動行列
	t = DirectX::XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);
	//ワールド行列
	w = s * r * t;

	DirectX::XMStoreFloat4x4(&mWorld, w);
}

