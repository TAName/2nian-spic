#pragma once
#include"vector.h"

class Obj3D
{
public:
	Obj3D();
	//ワールド行列計算関数
	void CalculateTransform();
	//setter
	void SetPosition(const VECTOR3F& position) { mPosition = position; }
	void SetScale(const VECTOR3F& scale) { mScale = scale; }
	void SetAngle(const VECTOR3F& angle) { mAngle = angle; }
	void SetWorld(const FLOAT4X4& world) { mWorld = world; }
	//getter
	const VECTOR3F& GetPosition() { return mPosition; }
	const VECTOR3F& GetScale() { return mScale; }
	const VECTOR3F& GetAngle() { return mAngle; }
	const FLOAT4X4& GetWorld() { return mWorld; }

private:
	VECTOR3F mPosition;
	VECTOR3F mScale;
	VECTOR3F mAngle;
	FLOAT4X4 mWorld;
};