#include "scene_over.h"
#include"scene_manager.h"
#include"key_board.h"
#include"blend_state.h"
#include"sound_manager.h"
std::unique_ptr<blend_state> blend_over;
SceneOver::SceneOver(ID3D11Device* device)
{
	gameover = std::make_unique<Sprite>(device, L"Data/image/game_over.png");
	stage_select_title01 = std::make_unique<Sprite>(device, L"Data/image/stage_select_title_font01.png");
	stage_select_title02 = std::make_unique<Sprite>(device, L"Data/image/stage_select_title_font02.png");
	blend_over = std::make_unique<blend_state>(device, BLEND_MODE::ALPHA);
	titleorselect = 0;
	pSoundManager.Play(SoundManager::SOUND::BGM_OVER, true);
	pSoundManager.SetVolume(SoundManager::SOUND::BGM_OVER, 1.0f);
}

void SceneOver::Update(float elapsed_time)
{
	/*if (pKeyBoad.RisingState(KeyLabel::SPACE))
	{
		pSceneManager.ChangeScene(SCENETYPE::CLEAR);
		return;
	}*/
	
	if (titleorselect == 0 && pKeyBoad.RisingState(KeyLabel::SPACE))
	{
		pSoundManager.Stop(SoundManager::SOUND::BGM_OVER);

		pSoundManager.Play(SoundManager::SOUND::Decision);
		pSoundManager.SetVolume(SoundManager::SOUND::Decision, 1.0f);


		pSceneManager.ChangeScene(SCENETYPE::GAME);
		return;
	}
	if (titleorselect == 1 && pKeyBoad.RisingState(KeyLabel::SPACE))
	{
		pSoundManager.Stop(SoundManager::SOUND::BGM_OVER);

		pSoundManager.Play(SoundManager::SOUND::Decision);
		pSoundManager.SetVolume(SoundManager::SOUND::Decision, 1.0f);


		pSceneManager.ChangeScene(SCENETYPE::TITLE);
		return;
	}
	//���
	if (titleorselect==0 && pKeyBoad.RisingState(KeyLabel::RIGHT))
	{
		titleorselect = 1;
	}
	if (titleorselect == 1 && pKeyBoad.RisingState(KeyLabel::LEFT))
	{
		titleorselect = 0;
	}
}

void SceneOver::Render(ID3D11DeviceContext* context, float elapsed_time)
{
	blend_over->activate(context);
	
	gameover->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
	if(titleorselect == 0)stage_select_title01->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
	if (titleorselect == 1)stage_select_title02->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
	blend_over->deactivate(context);
}

SceneOver::~SceneOver()
{
}
