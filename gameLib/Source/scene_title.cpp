#include "scene_title.h"
#include"key_board.h"
#include"scene_manager.h"
#include"blend_state.h"
#include"camera_manager.h"
#include"sound_manager.h"
#include"fado_out.h"

std::unique_ptr<blend_state> blend;

SceneTitle::SceneTitle(ID3D11Device* device)
{
	loading_thread = std::make_unique<std::thread>([&](ID3D11Device* device)
	{
		std::lock_guard<std::mutex> lock(loading_mutex);
		std::unique_ptr<ModelData>playerData = std::make_unique<ModelData>("Data/FBX/Player_anim/Player_anim.fbx", false);
		std::shared_ptr<ModelResource>playerResource = std::make_shared<ModelResource>(device, std::move(playerData));
		character = std::make_unique<Character>(playerResource);
		character->SetScale(VECTOR3F(1, 1, 1));
		character->SetAngle(VECTOR3F(DirectX::XMConvertToRadians(90), DirectX::XMConvertToRadians(140), 0));
		character->GetModel()->PlayAnimation(2, true);
		renderer = std::make_unique<ModelRenderer>(device);
		pCamera.CreateCamera();
		pCamera.GetCamera()->SetEye(VECTOR3F(0, 30, -100));
		pCamera.GetCamera()->SetFocus(VECTOR3F(0, 0, 0));
		pCamera.GetCamera()->SetUp(VECTOR3F(0, 1, 0));
		}, device);

	title = std::make_unique<Sprite>(device, L"Data/image/title.png");
	title_push_space = std::make_unique<Sprite>(device, L"Data/image/title_pushspace.png");
	siro = std::make_unique<Sprite>(device, L"Data/image/siro.png");
	blend = std::make_unique<blend_state>(device, BLEND_MODE::ALPHA);
	push_count = 0;
	pSoundManager.Play(SoundManager::SOUND::BGM_Title, true);
	pSoundManager.SetVolume(SoundManager::SOUND::BGM_Title, 1.0f);
}

void SceneTitle::Update(float elapsed_time)
{
	if (IsNowLoading())
	{
		return;
	}
	EndLoading();
	if (!pFadoOut.GetFado())
	{
		if (pKeyBoad.RisingState(KeyLabel::SPACE))
		{
			pSoundManager.Stop(SoundManager::SOUND::BGM_Title);
			pSoundManager.Play(SoundManager::SOUND::Decision);
			pSoundManager.SetVolume(SoundManager::SOUND::Decision, 1.0f);
			pFadoOut.SetNextScene(SCENETYPE::GAME);
		}
	}
	if (pFadoOut.Update(elapsed_time))return;
	character->GetModel()->UpdateAnimation(elapsed_time);
	character->CalculateMultipleTransform();
	pCamera.Update(elapsed_time);
}

void SceneTitle::Render(ID3D11DeviceContext* context, float elapsed_time)
{
	if (IsNowLoading())
	{
		return;
	}
	FLOAT4X4 view_projection;
	DirectX::XMMATRIX C = DirectX::XMMatrixSet(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);

	DirectX::XMStoreFloat4x4(&view_projection, C * DirectX::XMLoadFloat4x4(&pCamera.GetCamera()->GetView()) * DirectX::XMLoadFloat4x4(&pCamera.GetCamera()->GetProjection()));

	blend->activate(context);
	pFadoOut.Render(context);
	siro->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1024, 1024), 0, VECTOR4F(0, 1, 1, 0.4f));

	title->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
	if (push_count++ % 35 >= 14)title_push_space->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
	blend->deactivate(context);
	renderer->Begin(context, view_projection, VECTOR4F(0, 0.75f, 1, 0));
	renderer->Draw(context, *character->GetModel());
	renderer->End(context);
	blend->activate(context);
	pFadoOut.Render(context);
	blend->deactivate(context);

}

SceneTitle::~SceneTitle()
{
}
