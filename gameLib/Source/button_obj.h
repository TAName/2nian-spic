#pragma once
#include"static_obj.h"

class ButtonObj :public StaticObj
{
public:
	ButtonObj(std::shared_ptr<StaticMesh>mesh,int gateNo=0);
	//setter
	void SetPushTime(const float time) { pushTime = time; }
	void SetPushFlag(const bool flag) { mPushFlag = flag; }
	//getter
	const bool GetPushFlag() {return mPushFlag;}
	const int GetGateNo() { return mGateNo; }
	const float GetPushTime() { return pushTime; }
private:
	bool mPushFlag;
	int mGateNo;
	float pushTime;
};