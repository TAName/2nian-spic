#include "static_mesh.hlsli"

VS_OUT main(float4 position : POSITION, float3 normal : NORMAL, float2 texcoord : TEXCOORD/*UNIT.13*/)
{
	VS_OUT vout;
	vout.position = mul(position, wvp);
	//vout.position = mul(vout.position, view);
	//vout.position = mul(vout.position, projection);

	float3 N = normalize(mul(normal, (float3x3)world));
	float3 L = normalize(-lightDirection.xyz);
	float d = dot(L, N);
	vout.color.xyz = materialColor.xyz * max(0, d);
	vout.color.w = materialColor.w;

	vout.texcoord = texcoord;

	return vout;
}
