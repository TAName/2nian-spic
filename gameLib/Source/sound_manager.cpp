#include"sound_manager.h"
#if SOUNDMODO

SoundManager::SoundManager()
{
	SoundData sounddata[] =
	{
		{ SOUND::BGM_Title,"Data/sounds/title/title02.wav" },
		{ SOUND::BGM_Loading,"Data/sounds/result.wav" },
		{ SOUND::BGM_Select,"Data/sounds/game/select.wav" },
		{ SOUND::BGM_Game,"Data/sounds/game/game.wav" },
		{ SOUND::BGM_CLEAR,"Data/sounds/clear/clear.wav" },
		{ SOUND::BGM_OVER,"Data/sounds/over/over.wav" },
		{ SOUND::Decision,"Data/sounds/decision.wav" },
		{ SOUND::Button,"Data/sounds/button/button.wav" },
	};
	int size = sizeof(sounddata) / sizeof(SoundData);
	for (int i = 0; i < size; i++)
	{
		mSound.emplace_back(std::make_unique<Sound>(sounddata[i].soundfile));
	}
}
#else
SoundManager::SoundManager()
{
	DirectX::AUDIO_ENGINE_FLAGS flags = DirectX::AudioEngine_Default;

#ifdef _DEBUG
	flags = flags | DirectX::AudioEngine_Debug;
#endif

	audioEngine.reset(new DirectX::AudioEngine(flags));

	SoundData sounddata[] =
	{
		{ SOUND::BGM_Title,L"Data/sounds/result.wav" },
	};
	int size = sizeof(sounddata) / sizeof(SoundData);
	for (int i = 0; i < size; i++)
	{
		mSound.push_back(std::make_unique<Sound>(sounddata[i].soundfile, audioEngine.get()));
	}
}

#endif