#include "fado_out.h"
#include"scene_manager.h"


bool FadoOut::Update(float elapasd_time)
{
	if (!fadoFlag)
	{
		fadoEndFlag = false;

		return false;
	}
	if (color.w < 1.f)
	{
		color.w += elapasd_time;
		if (color.w >= 1.f)color.w = 1.f;
	}
	else
	{
		if(nextScene!=-1)fadoEndFlag = true;

		switch (nextScene)
		{
		case SCENETYPE::TITLE:
			pSceneManager.ChangeScene(SCENETYPE::TITLE);
			break;
		case SCENETYPE::GAME:
			pSceneManager.ChangeScene(SCENETYPE::GAME);
			break;
		case SCENETYPE::OVER:
			pSceneManager.ChangeScene(SCENETYPE::OVER);
			break;
		case SCENETYPE::CLEAR:
			pSceneManager.ChangeScene(SCENETYPE::CLEAR);
			break;

		}
		nextScene = -1;
		fadoFlag = false;
		color.w = 0.f;
	}
	return fadoEndFlag;
}

void FadoOut::Render(ID3D11DeviceContext* context)
{
	fado->Render(context, VECTOR2F(0, 0), VECTOR2F(1920, 1080), VECTOR2F(0, 0), VECTOR2F(1024, 1024), 0, color);
}

void FadoOut::Load(ID3D11Device* device)
{
	fado = std::make_unique<Sprite>(device, L"Data/image/siro.png");
}
