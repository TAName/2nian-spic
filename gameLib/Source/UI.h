#pragma once
#include"sprite.h"
#include<memory>
#include<array>

class UI
{
public:
	//UI(ID3D11Device* device);
	void Update(float elapsed_time);
	void Init(ID3D11Device* device);
	void Render(ID3D11DeviceContext* devicecontext);
	void Set_O2(const float o2) { bar = o2; }
	std::array<int, 4>get_time() { return { time_x[0],time_x[1],time_x[2],time_x[3] }; }
	static UI& GetInctance()
	{
		static UI ui;
		return ui;
	}
private:
	UI() {};
	float count[4];
	int time_x[4];
	float bar;
	float bar_width;
	std::unique_ptr<Sprite>time;
	std::unique_ptr<Sprite>gauge_frame;
	std::unique_ptr<Sprite>gauge_bar;

};