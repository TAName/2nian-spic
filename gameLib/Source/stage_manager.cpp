#include "stage_manager.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#include"imgui_stringvector_combo.h"
#endif
#include"framework.h"
#include"obj_manager.h"
#include"key_board.h"
#include"camera_manager.h"
#include"scene_manager.h"
#include"sound_manager.h"
#include"fado_out.h"

StageManager* StageManager::manager = nullptr;
/**************************************************************/
//コンストラクタ
/*********************************************************/
StageManager::StageManager(ID3D11Device* device) :meshNo(0), mStageNo(0), selectFlag(false), editorFlag(false), nowModo(1), modoSelectFlag(false), selectMoveTimer(0), mBeforeSelectNo(mStageNo)
, mStartCreatePosition(0, 0, 0), dragFlag(false), dragNo(-1), gateNo(0), objSetModo(0)
{
	meshName.push_back("floor");
	meshName.push_back("wall");
	meshName.push_back("button_base");
	meshName.push_back("button");
	meshName.push_back("door_frame");
	meshName.push_back("door");
	meshName.push_back("rise_object");
	meshName.push_back("ceiling");
	meshName.push_back("000_cube");
	const char* fbxPas = "Data/FBX/";
	for (int i = 0;i < static_cast<int>(meshName.size());i++)
	{
		std::string  fileName = { fbxPas };
		fileName += meshName[i];
		fileName += "/";
		fileName += meshName[i];
		fileName += ".fbx";
		mMeshes.push_back(std::make_shared<StaticMesh>(device, fileName.c_str()));
	}
	primitive = std::make_shared<GeometricSphere>(device, 32, 16);

	//mMeshes.push_back(std::make_shared<StaticMesh>(device, "Data/FBX/Arrow/arrow.fbx"));
	//mMeshes.push_back(std::make_shared<StaticMesh>(device, "Data/FBX/source/Dragon_ver2.fbx"));
	mArrow = std::make_unique<DragOperation>(device);
	mHitArea = std::make_unique<HitAreaSet>(device, meshName.size());
	SetStage();
	selectFrameSprite = std::make_unique<Sprite>(device, L"Data/image/stage_select.png");
	selectNumberSprite = std::make_unique<Sprite>(device, L"Data/image/font_black.png");
	selectArrowSprite = std::make_unique<Sprite>(device, L"Data/image/arrow108.png");
}
/*************************************************************/
//ファイル操作
/************************************************************/
void StageManager::SetStage()
{
	bool end = false;
	FILE* fp;
	const char* filePas = { "Data/file/" };
	const char* startPas = { "stage" };
	int i = 0;
	while (!end)
	{
		std::string stage = { filePas };
		std::string s = { startPas };
		s += std::to_string(i);
		stage += s;
		stage += ".bin";
		if (fopen_s(&fp, stage.c_str(), "rb") == 0)
		{

			stageName.push_back(s);
			fclose(fp);
		}
		else
		{
			end = true;
		}
		i++;
	}
	const char* hitFile = { "Data/file/hitArea" };
	for (int i = 0;i < static_cast<int>(meshName.size());i++)
	{
		std::string file = { hitFile };
		file += std::to_string(i);
		file += ".bin";
		if (fopen_s(&fp, file.c_str(), "rb") == 0)
		{
			VECTOR3F area;
			fread(&area, sizeof(VECTOR3F), 1, fp);
			mHitArea->SetArea(area, i);
			fclose(fp);
		}
	}
}
void StageManager::Load(const int stageNo)
{
	std::string fileName = { "Data/file/stage" };
	fileName += std::to_string(stageNo);
	fileName += ".bin";
	FILE* fp;
	mObjects.clear();
	std::vector<SaveData>data;
	int size;
	if (fopen_s(&fp, fileName.c_str(), "rb") == 0)
	{
		fread(&size, sizeof(int), 1, fp);
		if (size > 0)
		{
			data.resize(size);
			fread(&data[0], sizeof(SaveData), size, fp);
			for (auto& d : data)
			{
				CreateObj(d.meshesNo, d.position, d.scale, d.angle);
			}
		}
		fclose(fp);
	}
}
void StageManager::Save(const int stageNo)
{
	std::string fileName = { "Data/file/stage" };
	fileName += std::to_string(stageNo);
	fileName += ".bin";
	FILE* fp;
	std::vector<SaveData>data;
	fopen_s(&fp, fileName.c_str(), "wb");
	int size = static_cast<int>(mObjects.size());
	fwrite(&size, sizeof(int), 1, fp);
	if (size > 0)
	{
		for (auto& object : mObjects)
		{
			data.emplace_back();
			data.back().angle = object->GetAngle();
			data.back().position = object->GetPosition();
			data.back().scale = object->GetScale();
			data.back().meshesNo = object->GetMeshNo();
		}
		fwrite(&data[0], sizeof(SaveData), size, fp);
	}
	fclose(fp);
	const char* hitFile = { "Data/file/hitArea" };
	for (int i = 0;i < static_cast<int>(meshName.size());i++)
	{
		std::string file = { hitFile };
		file += std::to_string(i);
		file += ".bin";
		fopen_s(&fp, file.c_str(), "wb");

		VECTOR3F area = mHitArea->GetArea()[i];
		fwrite(&area, sizeof(VECTOR3F), 1, fp);
		mHitArea->SetArea(area, i);
		fclose(fp);

	}


}
/**************************************************************/
//マウスのワールド座標変換関数
/**************************************************************/
void StageManager::ScreeenToWorld(VECTOR3F* worldPosition, const VECTOR3F& screenPosition)
{
	ID3D11DeviceContext* context = pSceneManager.GetDeviceContext();
	D3D11_VIEWPORT viewport;
	UINT num_viewports = 1;
	context->RSGetViewports(&num_viewports, &viewport);
	// ビューポート
	float viewportX = 0.0f;
	float viewportY = 0.0f;
	float viewportW = static_cast<float>(viewport.Width);
	float viewportH = static_cast<float>(viewport.Height);
	float viewportMinZ = 0.0f;
	float viewportMaxZ = 1.0f;
	// ビュー行列
	DirectX::XMMATRIX V = DirectX::XMLoadFloat4x4(&pCamera.GetCamera()->GetView());
	// プロジェクション行列
	DirectX::XMMATRIX P = DirectX::XMLoadFloat4x4(&pCamera.GetCamera()->GetProjection());
	// ワールド行列
	DirectX::XMMATRIX W = DirectX::XMMatrixIdentity();//移動成分はいらないので単位行列を入れておく。
	// スクリーン座標からNDC座標へ変換
	DirectX::XMVECTOR NDCPos = DirectX::XMVectorSet(
		2.0f * (screenPosition.x - viewportX) / viewportW - 1.0f,
		1.0f - 2.0f * (screenPosition.y - viewportY) / viewportH,
		(screenPosition.z - viewportMinZ) / (viewportMaxZ - viewportMinZ),
		1.0f);
	// NDC座標からワールド座標へ変換
	DirectX::XMMATRIX WVP = W * V * P;
	DirectX::XMMATRIX IWVP = DirectX::XMMatrixInverse(nullptr, WVP);
	DirectX::XMVECTOR WPos = DirectX::XMVector3TransformCoord(NDCPos, IWVP);
	DirectX::XMStoreFloat3(worldPosition, WPos);
}
void StageManager::MouseToWorld()
{
	POINT cursor;
	GetCursorPos(&cursor);
	ScreenToClient(Framework::Instance().GetHwnd(), &cursor);
	VECTOR3F positionNear = VECTOR3F(static_cast<float>(cursor.x), static_cast<float>(cursor.y), 0);
	VECTOR3F positionFar = VECTOR3F(static_cast<float>(cursor.x), static_cast<float>(cursor.y), 1);

	ScreeenToWorld(&mNearPosition, positionNear);
	ScreeenToWorld(&mFarPosition, positionFar);
}
void StageManager::CreateObj(int number, const VECTOR3F& position, const VECTOR3F& scale, const VECTOR3F& angle)
{
	switch (number)
	{
	case MESHTYPE::BUTTON:
	case MESHTYPE::BUTTON + 1:
		mButton.push_back(std::make_shared<StageButton>(mMeshes[MESHTYPE::BUTTON], mMeshes[MESHTYPE::BUTTON + 1], static_cast<int>(MESHTYPE::BUTTON), gateNo));
		break;
	case MESHTYPE::GORL:
	case MESHTYPE::GORL + 1:
		mGate.push_back(std::make_shared<StageGate>(mMeshes[MESHTYPE::GORL], mMeshes[MESHTYPE::GORL + 1], static_cast<int>(MESHTYPE::GORL), mGate.size()));
		break;
	case MESHTYPE::MOVE:
		mMoves.push_back(std::make_shared<StageMove>(mMeshes[number], number));
		break;
	case 8:
		mAreas.push_back(std::make_shared<SupplyArea>(mMeshes[number], primitive, number));
		mAreas.back()->GetSupplyObj()->SetLength(1.0f);
		break;
	default:
		mStage.push_back(std::make_shared<Stage>(mMeshes[number], number));
		break;
	}
	mObjects.back()->SetScale(scale);
	mObjects.back()->SetAngle(angle);
	mObjects.back()->SetPosition(position);
	mObjects.back()->SetHitArea(mHitArea->GetArea()[number]);

}
/********************************************************/
//エディター
/********************************************************/
bool StageManager::Editor()
{
	if (nowModo == 0)return true;
#ifdef USE_IMGUI
	ImGui::Begin(stageName[mStageNo].c_str());
	if (!editorFlag)
	{
		MouseToWorld();
		if (!mArrow->Editor(mNearPosition, mFarPosition)&&!mHitArea->Editor())
		{
			if (pKeyBoad.FallingState(VK_LBUTTON) && !pKeyBoad.PressedState(VK_MENU))
			{
				int num = 0;
				int getNum = -1;
				float maxLength = pCamera.GetCamera()->GetFar();
				float length = 0;
				for (auto& object : mObjects)
				{
					VECTOR3F position, normal;

					if (object->RayPick(mNearPosition, mFarPosition, &position, &normal, &length) != -1)
					{
						if (maxLength >= length)
						{
							getNum = num;
							maxLength = length;
						}
					}
					num++;
				}
				if (pKeyBoad.PressedState(KeyLabel::LSHIFT) && getNum != -1)
				{
					dragNo = getNum;
				}
				if (pKeyBoad.PressedState(KeyLabel::LCONTROL) && getNum != -1)
				{
					dragNo = getNum;
					if (objSetModo == 0)mArrow->SetDragObj(mObjects[dragNo]);
					else if (objSetModo == 1)mHitArea->SetDragObj(mObjects[dragNo], mObjects[dragNo]->GetMeshNo());
				}

			}
		}

		ImGui::Text("obj %d", mObjects.size());
		ImGui::Combo("meshName", &meshNo, vector_getter, static_cast<void*>(&meshName), static_cast<int>(meshName.size()));
		static int startType = 0;
		ImGui::RadioButton("default", &startType, 0);
		ImGui::RadioButton("before", &startType, 1);
		ImGui::RadioButton("selectObj", &startType, 2);
		ImGui::Text("dragNo:%d", dragNo);
		ImGui::SliderInt("objSetModo", &objSetModo, 0, 1);
		if (meshNo < MESHTYPE::GORL && meshNo >= MESHTYPE::BUTTON && mGate.size()>0)
		{
			ImGui::SliderInt("gateNo", &gateNo, 0, mGate.size() - 1);
		}
		if (ImGui::Button("create"))
		{
			switch (startType)
			{
			case 0:mStartCreatePosition = pCamera.GetCamera()->GetFocus();break;
			case 1:
				if (mObjects.size() > 0)mStartCreatePosition = mObjects.back()->GetPosition();
				else mStartCreatePosition = pCamera.GetCamera()->GetFocus();
				break;
			case 2:mStartCreatePosition = mObjects[dragNo]->GetPosition();break;
			}

			CreateObj(meshNo, mStartCreatePosition);
		}
		if (ImGui::Button("save"))
		{
			Save(mStageNo);
		}
		if (ImGui::Button("load"))
		{
			pObjManager->ClearStage();
			mButton.clear();
			mGate.clear();
			mStage.clear();
			mMoves.clear();
			mAreas.clear();
			mArrow->SetOperationFlag(false);
			Load(mStageNo);
		}
		if (ImGui::Button("clear"))
		{
			mObjects.clear();
			pObjManager->ClearStage();
			mButton.clear();
			mGate.clear();
			mStage.clear();
			mMoves.clear();
			mAreas.clear();

		}
		if (ImGui::Button("play"))
		{
			editorFlag = true;
		}

	}
	else
	{
		if (ImGui::Button("editor"))
		{
			editorFlag = false;
		}
	}
	if (ImGui::Button("end"))
	{
		ResetModo();
		mObjects.clear();
		mButton.clear();
		mGate.clear();
		mStage.clear();
		mMoves.clear();
		mAreas.clear();

		pObjManager->ClearStage();
	}
	ImGui::End();
#endif
	return editorFlag;
}
//ステージセレクトエディター
void StageManager::StageEditor()
{
#ifdef USE_IMGUI
	ImGui::Begin("stage select");
	ImGui::Combo("stage", &mStageNo, vector_getter, static_cast<void*>(&stageName), static_cast<int>(stageName.size()));

	if (ImGui::Button("Load"))
	{
		Load(mStageNo);
		selectFlag = true;
	}
	if (ImGui::Button("CreateStage"))
	{
		int stageMax = stageName.size();
		std::string s = { "stage" };
		s += std::to_string(stageMax);
		stageName.push_back(s);
		mObjects.clear();
		mStageNo = stageMax;
		Save(stageMax);

		selectFlag = true;
	}
	ImGui::End();
#endif
	}


bool StageManager::SelectModo()
{
	if (modoSelectFlag)return true;
#ifdef USE_IMGUI
	ImGui::Begin("game modo select");
	ImGui::Combo("game modo", &nowModo, "play game\0editor\0\0");
	modoSelectFlag = ImGui::Button("decision");
	ImGui::End();
#else
	modoSelectFlag = true;
#endif
	return modoSelectFlag;
}
/**************************************************/
//セレクト画面
/***************************************************/
bool StageManager::Select(float elapasd_time)
{
	if (pFadoOut.GetFado() && pFadoOut.GetNextScene() == -1)
	{
		return false;
	}
	if (selectFlag)return true;
	if (!SelectModo())return false;

#ifdef USE_IMGUI
	switch (nowModo)
	{
	case MODO::GAME:
		selectFlag = StageSelect(elapasd_time);
		break;
	case MODO::EDITOR:
		StageEditor();
		break;
	}
#else
	nowModo = 0;
	selectFlag = StageSelect(elapasd_time);
#endif
	if (selectFlag)
	{
		pSoundManager.Stop(SoundManager::SOUND::BGM_Select);
		pSoundManager.Play(SoundManager::SOUND::BGM_Game, true);
		pSoundManager.SetVolume(SoundManager::SOUND::BGM_Game, 1.0f);
	}
	return false;
}

bool StageManager::StageSelect(float elapasd_time)
{
#ifdef USE_IMGUI
	ImGui::Begin("stageNo");
	ImGui::Text("nowStageNo:%d", mStageNo);
	ImGui::Text("maxStageNo:%d", stageName.size() - 1);
	ImGui::End();
#endif
	if (mStageNo == mBeforeSelectNo)
	{
		if (pKeyBoad.RisingState(KeyLabel::LEFT) && mStageNo > 0)
		{
			mStageNo--;
		}

		if (pKeyBoad.RisingState(KeyLabel::RIGHT) && mStageNo < static_cast<int>(stageName.size()) - 1)
		{
			mStageNo++;
		}
		if (pKeyBoad.RisingState(KeyLabel::SPACE))
		{
			Load(mStageNo);
			pSoundManager.Play(SoundManager::SOUND::Decision);
			pSoundManager.SetVolume(SoundManager::SOUND::Decision, 1.0f);
			pFadoOut.SetNextScene(-1);

			return true;
		}
	}
	else
	{
		if (mStageNo > mBeforeSelectNo)selectMoveTimer += elapasd_time;
		else selectMoveTimer -= elapasd_time;

		if (selectMoveTimer * selectMoveTimer >= 1.0f)
		{
			mBeforeSelectNo = mStageNo;
			selectMoveTimer = 0;
		}
	}
	return false;
}

bool StageManager::SelectRender(ID3D11DeviceContext* context)
{
	if (pFadoOut.GetFado() && pFadoOut.GetNextScene() == -1)
	{
	}

	else if (selectFlag)return selectFlag;
	D3D11_VIEWPORT viewport;
	UINT num_viewports = 1;
	context->RSGetViewports(&num_viewports, &viewport);
	VECTOR2F spritePosition[2];
	static VECTOR2F spriteArrowPosition[] = { VECTOR2F(viewport.Width * 0.05f, viewport.Height * 0.3f),VECTOR2F(viewport.Width * 0.85f, viewport.Height * 0.3f) };
	if (mStageNo == mBeforeSelectNo)
	{
		spritePosition[0] = VECTOR2F(viewport.Width * 0.2f, viewport.Height * 0.2f);

		if (mStageNo > 0)
		{
			selectArrowSprite->Render(context, spriteArrowPosition[0], VECTOR2F(244, 488), VECTOR2F(0, 0), VECTOR2F(244, 488), 180, VECTOR4F(1, 1, 1, 0.5f));
		}
		if (mStageNo < static_cast<int>(stageName.size())-1)
		{
			selectArrowSprite->Render(context, spriteArrowPosition[1], VECTOR2F(244, 488), VECTOR2F(0, 0), VECTOR2F(244, 488), 0, VECTOR4F(1, 1, 1, 0.5f));
		}
	}
	else
	{

		if (mStageNo > mBeforeSelectNo)
		{
			spritePosition[0] = VECTOR2F(viewport.Width * 0.2f + viewport.Width * 1.2f - viewport.Width * 1.2f * selectMoveTimer, viewport.Height * 0.2f);
		}
		else
		{
			spritePosition[0] = VECTOR2F(viewport.Width * 0.2f - viewport.Width * 1.2f - viewport.Width * 1.2f * selectMoveTimer, viewport.Height * 0.2f);
		}
		spritePosition[1] = VECTOR2F(viewport.Width * 0.2f - viewport.Width * 1.2f * selectMoveTimer, viewport.Height * 0.2f);

		selectFrameSprite->Render(context, spritePosition[1], VECTOR2F(viewport.Width * 0.6f, viewport.Height * 0.6f), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
		selectNumberSprite->Render(context, spritePosition[1] + VECTOR2F(viewport.Width * 0.18f, viewport.Height * 0.102f), VECTOR2F(64, 64) * 0.65f, VECTOR2F(64 * static_cast<float>(mBeforeSelectNo), 0), VECTOR2F(64, 64), 0);

	}
	selectFrameSprite->Render(context, spritePosition[0], VECTOR2F(viewport.Width * 0.6f, viewport.Height * 0.6f), VECTOR2F(0, 0), VECTOR2F(1920, 1080), 0);
	selectNumberSprite->Render(context, spritePosition[0] + VECTOR2F(viewport.Width * 0.18f, viewport.Height * 0.102f), VECTOR2F(64, 64) * 0.65f, VECTOR2F(64.f * static_cast<float>(mStageNo), 0), VECTOR2F(64, 64), 0);
	pFadoOut.Render(context);
	return false;
}



/*****************************************************/
//更新
/*****************************************************/
void StageManager::Update(float elapasd_time)
{
	for (auto& stage : mStage)
	{

		stage->Update(elapasd_time);
	}
	for (auto& button : mButton)
	{
		button->Update(elapasd_time);
	}
	for (auto& gate : mGate)
	{
		gate->Update(elapasd_time);
	}
	for (auto& move : mMoves)
	{
		move->Update(elapasd_time);
	}
	for (auto& area : mAreas)
	{
		area->Update(elapasd_time);
	}
	auto remove01 = std::remove_if(mStage.begin(), mStage.end(), [](std::shared_ptr<Stage>obj) {if (obj->GetStageObj()->GetExist())return false;else return true;});
	mStage.erase(remove01, mStage.end());
	auto remove02 = std::remove_if(mButton.begin(), mButton.end(), [](std::shared_ptr<StageButton>obj) {if (obj->GetStageObj()->GetExist())return false;else return true;});
	mButton.erase(remove02, mButton.end());
	auto remove03 = std::remove_if(mGate.begin(), mGate.end(), [](std::shared_ptr<StageGate>obj) {if (obj->GetStageObj()->GetExist())return false;else return true;});
	mGate.erase(remove03, mGate.end());
	auto remove04 = std::remove_if(mObjects.begin(), mObjects.end(), [](std::shared_ptr<StageObj>obj) {if (obj->GetExist())return false;else return true;});
	mObjects.erase(remove04, mObjects.end());
	auto remove05 = std::remove_if(mMoves.begin(), mMoves.end(), [](std::shared_ptr<StageMove>obj) {if (obj->GetObj()->GetExist())return false;else return true;});
	mMoves.erase(remove05, mMoves.end());
	auto remove06 = std::remove_if(mAreas.begin(), mAreas.end(), [](std::shared_ptr<SupplyArea>obj) {if (obj->GetSupplyObj()->GetLength()>0.0f)return false;else return true;});
	mAreas.erase(remove06, mAreas.end());


}
/*********************************************************/
//デストラクタ
/*********************************************************/
StageManager::~StageManager()
{
	mObjects.clear();
	mMeshes.clear();
	mButton.clear();
	mGate.clear();
	mStage.clear();
	mMoves.clear();
}
