#include "static_mesh.h"
#include <Shlwapi.h>
#include <fstream>
#include <vector>
#include <functional>

//#include <fbxsdk.h>
//using namespace fbxsdk;

#include "misc.h"
#include"texture.h"
#include"shader.h"

void fbxamatrix_to_xmfloat4x4(const FbxAMatrix& fbxamatrix, DirectX::XMFLOAT4X4& xmfloat4x4)
{
	for (int row = 0; row < 4; row++)
	{
		for (int column = 0; column < 4; column++)
		{
			xmfloat4x4.m[row][column] = static_cast<float>(fbxamatrix[row][column]);
		}
	}
}

StaticMesh::StaticMesh(ID3D11Device* device, const char* fileName)
{
	std::string fbxName = fileName;
    size_t engineer = fbxName.find_last_of(".") + 1;
    std::string name = fbxName.substr(0, engineer);
	name += "bin";
	if (PathFileExistsA((name).c_str()))
	{
		std::ifstream ifs;
		
		ifs.open((name).c_str(), std::ios::binary);
		cereal::BinaryInputArchive i_archive(ifs);
		i_archive(*this);

	}
	else
	{
		CreateMesh(device, fileName);
		std::ofstream ofs;
		ofs.open((name).c_str(), std::ios::binary);
		cereal::BinaryOutputArchive o_archive(ofs);
		o_archive(*this);
	}
	CreateBuffers(device);
	CreateShaderResourceView(device);
}


void StaticMesh::CreateMesh(ID3D11Device* device, const char* fileName)
{
	// Create the FBX SDK manager
	FbxManager* manager = FbxManager::Create();

	// Create an IOSettings object. IOSROOT is defined in Fbxiosettingspath.h.
	manager->SetIOSettings(FbxIOSettings::Create(manager, IOSROOT));

	// Create an importer.
	FbxImporter* importer = FbxImporter::Create(manager, "");

	// Initialize the importer.
	bool importStatus = false;
	importStatus = importer->Initialize(fileName, -1, manager->GetIOSettings());
	_ASSERT_EXPR_A(importStatus, importer->GetStatus().GetErrorString());

	// Create a new scene so it can be populated by the imported file.
	FbxScene* scene = FbxScene::Create(manager, "");

	// Import the contents of the file into the scene.
	importStatus = importer->Import(scene);
	_ASSERT_EXPR_A(importStatus, importer->GetStatus().GetErrorString());

	// Convert mesh, NURBS and patch into triangle mesh
	fbxsdk::FbxGeometryConverter geometryConverter(manager);
	geometryConverter.Triangulate(scene, /*replace*/true);

	// Fetch node attributes and materials under this node recursively. Currently only mesh.	
	std::vector<FbxNode*> fetchedMeshes;
	std::function<void(FbxNode*)> traverse = [&](FbxNode* node)
	{
		if (node)
		{
			FbxNodeAttribute* fbxNodeAttribute = node->GetNodeAttribute();
			if (fbxNodeAttribute)
			{
				switch (fbxNodeAttribute->GetAttributeType())
				{
				case FbxNodeAttribute::eMesh:
					fetchedMeshes.push_back(node);
					break;
				}
			}
			for (int i = 0; i < node->GetChildCount(); i++)
			{
				traverse(node->GetChild(i));
			}
		}
	};
	traverse(scene->GetRootNode());

	meshes.resize(fetchedMeshes.size());

	//FbxMesh *fbx_mesh = fetched_meshes.at(0)->GetMesh();  // Currently only one mesh.
	for (size_t i = 0; i < fetchedMeshes.size(); i++)
	{
		FbxMesh* fbxMesh = fetchedMeshes.at(i)->GetMesh();
		Mesh& mesh = meshes.at(i);

		FbxAMatrix globalTransform = fbxMesh->GetNode()->EvaluateGlobalTransform(0);
		fbxamatrix_to_xmfloat4x4(globalTransform, mesh.globalTransform);

		// Fetch material properties.
		const int numberOfMaterials = fbxMesh->GetNode()->GetMaterialCount();

		//subsets.resize(number_of_materials);
		//subsets.resize(number_of_materials > 0 ? number_of_materials : 1);

		mesh.subsets.resize(numberOfMaterials > 0 ? numberOfMaterials : 1);
		for (int indexOfMaterial = 0; indexOfMaterial < numberOfMaterials; ++indexOfMaterial)
		{
			//subset &subset = subsets.at(index_of_material);
			Subset& subset = mesh.subsets.at(indexOfMaterial);

			const FbxSurfaceMaterial* surfaceMaterial = fbxMesh->GetNode()->GetMaterial(indexOfMaterial);

			const FbxProperty property = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sDiffuse);
			const FbxProperty factor = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sDiffuseFactor);
			if (property.IsValid() && factor.IsValid())
			{
				FbxDouble3 color = property.Get<FbxDouble3>();
				double f = factor.Get<FbxDouble>();
				subset.diffuse.color.x = static_cast<float>(color[0] * f);
				subset.diffuse.color.y = static_cast<float>(color[1] * f);
				subset.diffuse.color.z = static_cast<float>(color[2] * f);
				subset.diffuse.color.w = 1.0f;
			}
			if (property.IsValid())
			{
				const int numberOfTextures = property.GetSrcObjectCount<FbxFileTexture>();
				if (numberOfTextures)
				{
					const FbxFileTexture* fileTexture = property.GetSrcObject<FbxFileTexture>();
					if (fileTexture)
					{
						const char* filename = fileTexture->GetRelativeFileName();
						// Create "diffuse.shader_resource_view" from "filename".
						//wchar_t texture_unicode[256];
						//MultiByteToWideChar(CP_ACP, 0, filename, strlen(filename) + 1, texture_unicode, 1024);
						//D3D11_TEXTURE2D_DESC texture2d_desc;
						//load_texture_from_file(device, texture_unicode, subset.diffuse.shader_resource_view.GetAddressOf(), &texture2d_desc);

						wchar_t fbxUnicode[256];
						MultiByteToWideChar(CP_ACP, 0, fileName, static_cast<int>(strlen(fileName) + 1), fbxUnicode, 1024);
						wchar_t textureUnicode[256];
						MultiByteToWideChar(CP_ACP, 0, fileTexture->GetFileName(), static_cast<int>(strlen(fileTexture->GetFileName()) + 1), textureUnicode, 1024);
						combine_resource_path(textureUnicode, fbxUnicode, textureUnicode);

						subset.diffuse.textureName = textureUnicode;
						//material.texture_filename = texture_unicode;
					}
				}
			}
		}

		// Count the polygon count of each material
		if (numberOfMaterials > 0)
		{
			// Count the faces of each material
			const int numberOfPolygons = fbxMesh->GetPolygonCount();
			for (int indexOfPolygon = 0; indexOfPolygon < numberOfPolygons; ++indexOfPolygon)
			{
				const u_int materialIndex = fbxMesh->GetElementMaterial()->GetIndexArray().GetAt(indexOfPolygon);

				//subsets.at(material_index).index_count += 3;
				mesh.subsets.at(materialIndex).indexCount += 3;

			}

			// Record the offset (how many vertex)
			int offset = 0;

			//for (subset &subset : subsets)
			for (Subset& subset : mesh.subsets)
			{
				subset.indexStart = offset;
				offset += subset.indexCount;
				// This will be used as counter in the following procedures, reset to zero
				subset.indexCount = 0;
			}
		}

		// Fetch mesh data
		u_int vertexCount = 0;

		FbxStringList uvNames;
		fbxMesh->GetUVSetNames(uvNames);

		const FbxVector4* arrayOfControlPoints = fbxMesh->GetControlPoints();
		const int numberOfPolygons = fbxMesh->GetPolygonCount();
		mesh.indices.resize(numberOfPolygons * 3);
		for (int indexOfPolygon = 0; indexOfPolygon < numberOfPolygons; indexOfPolygon++)
		{
			// The material for current face.
			int indexOfMaterial = 0;
			if (numberOfMaterials > 0)
			{
				indexOfMaterial = fbxMesh->GetElementMaterial()->GetIndexArray().GetAt(indexOfPolygon);
			}

			// Where should I save the vertex attribute index, according to the material
			//subset &subset = subsets.at(index_of_material);
			Subset& subset = mesh.subsets.at(indexOfMaterial);
			const int indexOffset = subset.indexStart + subset.indexCount;
			Face f;
			f.materialIndex = indexOfMaterial;
			for (int indexOfVertex = 0; indexOfVertex < 3; indexOfVertex++)
			{
				Vertex vertex;
				const int indexOfControlPoint = fbxMesh->GetPolygonVertex(indexOfPolygon, indexOfVertex);
				vertex.position.x = static_cast<float>(arrayOfControlPoints[indexOfControlPoint][0]);
				vertex.position.y = static_cast<float>(arrayOfControlPoints[indexOfControlPoint][1]);
				vertex.position.z = static_cast<float>(arrayOfControlPoints[indexOfControlPoint][2]);

				if (fbxMesh->GetElementNormalCount() > 0)
				{
					FbxVector4 normal;
					fbxMesh->GetPolygonVertexNormal(indexOfPolygon, indexOfVertex, normal);
					vertex.normal.x = static_cast<float>(normal[0]);
					vertex.normal.y = static_cast<float>(normal[1]);
					vertex.normal.z = static_cast<float>(normal[2]);
				}

				if (fbxMesh->GetElementUVCount() > 0)
				{
					FbxVector2 uv;
					bool unmapped_uv;
					fbxMesh->GetPolygonVertexUV(indexOfPolygon, indexOfVertex, uvNames[0], uv, unmapped_uv);
					vertex.texcoord.x = static_cast<float>(uv[0]);
					vertex.texcoord.y = 1.0f - static_cast<float>(uv[1]);
				}
				//面ごとの頂点データ
				DirectX::XMVECTOR facePosition = DirectX::XMLoadFloat3(&vertex.position);
				DirectX::XMMATRIX fW = DirectX::XMLoadFloat4x4(&mesh.globalTransform);
				DirectX::XMVECTOR fLP = DirectX::XMVector3TransformCoord(facePosition, fW);
				DirectX::XMStoreFloat3(&f.position[indexOfVertex], fLP);
				mesh.vertices.push_back(vertex);
				mesh.indices.at(indexOffset + indexOfVertex) = static_cast<u_int>(vertexCount);
				vertexCount += 1;
			}
			subset.indexCount += 3;
			//面データの保存
			mesh.faces.push_back(f);
		}
		//create_buffers(device, vertices.data(), vertices.size(), indices.data(), indices.size());
	}
	manager->Destroy();

}

void StaticMesh::CreateBuffers(ID3D11Device* device)
{
	HRESULT hr;

	for (auto& mesh : meshes)
	{
		//頂点バッファの生成
		{
			D3D11_BUFFER_DESC desc = {};
			D3D11_SUBRESOURCE_DATA data = {};

			desc.ByteWidth = sizeof(Vertex) * mesh.vertices.size();
			//buffer_desc.Usage = D3D11_USAGE_DEFAULT;
			desc.Usage = D3D11_USAGE_DEFAULT;
			desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			desc.CPUAccessFlags = 0;
			desc.MiscFlags = 0;
			desc.StructureByteStride = 0;
			data.pSysMem = mesh.vertices.data();
			data.SysMemPitch = 0; //Not use for vertex buffers.mm 
			data.SysMemSlicePitch = 0; //Not use for vertex buffers.

			hr = device->CreateBuffer(&desc, &data, mesh.vertexBuffer.ReleaseAndGetAddressOf());
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		}
		//インテックスバッファの生成
		{
			D3D11_BUFFER_DESC desc = {};
			D3D11_SUBRESOURCE_DATA data = {};

			desc.ByteWidth = sizeof(u_int) * mesh.indices.size();
			desc.Usage = D3D11_USAGE_DEFAULT;
			desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			desc.CPUAccessFlags = 0;
			desc.MiscFlags = 0;
			desc.StructureByteStride = 0;
			data.pSysMem = mesh.indices.data();
			data.SysMemPitch = 0; //Not use for index buffers.
			data.SysMemSlicePitch = 0; //Not use for index buffers.
			hr = device->CreateBuffer(&desc, &data, mesh.indexBuffer.ReleaseAndGetAddressOf());
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		}
	}
}

void StaticMesh::CreateShaderResourceView(ID3D11Device* device)
{
	for (auto& mesh : meshes)
	{
		for (auto& subset : mesh.subsets)
		{
			if (wcscmp(subset.diffuse.textureName.c_str(), L"") != 0)
			{
				D3D11_TEXTURE2D_DESC texture2dDesc;
				load_texture_from_file(device, subset.diffuse.textureName.c_str(), subset.diffuse.mShaderResourceView.GetAddressOf(), &texture2dDesc);
			}
			if (!subset.diffuse.mShaderResourceView)
			{
				make_dummy_texture(device, subset.diffuse.mShaderResourceView.GetAddressOf());
			}
		}
	}
}

int StaticMesh::RayPick(const VECTOR3F& startPosition, const VECTOR3F& endPosition, VECTOR3F* outPosition, VECTOR3F* outNormal, float* outLength)
{
	int ret = -1;
	DirectX::XMVECTOR start = DirectX::XMLoadFloat3(&startPosition);
	DirectX::XMVECTOR end = DirectX::XMLoadFloat3(&endPosition);
	DirectX::XMVECTOR vec = DirectX::XMVectorSubtract(end, start);
	DirectX::XMVECTOR length = DirectX::XMVector3Length(vec);
	DirectX::XMVECTOR dir = DirectX::XMVector3Normalize(vec);
	float neart;
	DirectX::XMStoreFloat(&neart, length);

	DirectX::XMVECTOR position, normal;
	for (auto& mesh : meshes)
	{
		for (auto& face : mesh.faces)
		{
			//面頂点取得
			DirectX::XMVECTOR a = DirectX::XMLoadFloat3(&face.position[0]);
			DirectX::XMVECTOR b = DirectX::XMLoadFloat3(&face.position[1]);
			DirectX::XMVECTOR c = DirectX::XMLoadFloat3(&face.position[2]);
			//3辺算出
			DirectX::XMVECTOR ab = DirectX::XMVectorSubtract(b, a);
			DirectX::XMVECTOR bc = DirectX::XMVectorSubtract(c, b);
			DirectX::XMVECTOR ca = DirectX::XMVectorSubtract(a, c);
			//外積による法線算出
			DirectX::XMVECTOR n = DirectX::XMVector3Cross(ab, bc);
			//内積の結果がプラスならば裏向き
			DirectX::XMVECTOR dot = DirectX::XMVector3Dot(dir, n);
			float fdot;
			DirectX::XMStoreFloat(&fdot, dot);
			if (fdot >= 0)continue;
			//交点算出
			DirectX::XMVECTOR cp;
			DirectX::XMVECTOR as = DirectX::XMVectorSubtract(a, start);
			DirectX::XMVECTOR d1 = DirectX::XMVector3Dot(n, as);
			DirectX::XMVECTOR x = DirectX::XMVectorDivide(d1, dot);//割り算
			float xleng;
			DirectX::XMStoreFloat(&xleng, x);
			if (xleng < .0f || xleng > neart) continue;
			cp = DirectX::XMVectorAdd(DirectX::XMVectorMultiply(dir, x), start);//足し算、かけ算
			//内点判定
			DirectX::XMVECTOR v1 = DirectX::XMVectorSubtract(a, cp);
			DirectX::XMVECTOR temp = DirectX::XMVector3Cross(v1, ab);
			DirectX::XMVECTOR work = DirectX::XMVector3Dot(temp, n);
			float fwork;
			DirectX::XMStoreFloat(&fwork, work);
			if (fwork < 0.0f)continue;
			DirectX::XMVECTOR v2 = DirectX::XMVectorSubtract(b, cp);
			temp = DirectX::XMVector3Cross(v2, bc);
			work = DirectX::XMVector3Dot(temp, n);
			DirectX::XMStoreFloat(&fwork, work);
			if (fwork < 0.0f)continue;
			DirectX::XMVECTOR v3 = DirectX::XMVectorSubtract(c, cp);
			temp = DirectX::XMVector3Cross(v3, ca);
			work = DirectX::XMVector3Dot(temp, n);
			DirectX::XMStoreFloat(&fwork, work);
			if (fwork < 0.0f)continue;

			//情報保存
			position = cp;
			normal = n;
			neart = xleng;
			ret = face.materialIndex;
		}
	}
	if (ret != -1)
	{
		DirectX::XMStoreFloat3(outPosition, position);
		DirectX::XMStoreFloat3(outNormal, normal);
	}
	*outLength = neart;

	return ret;
}
/************************************************/
//             描画クラス
/************************************************/
MeshRender::MeshRender(ID3D11Device* device)
{
	HRESULT hr = S_OK;

	D3D11_INPUT_ELEMENT_DESC inputElementDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	create_vs_from_cso(device, "Data/shader/static_mesh_vs.cso", mVSShader.GetAddressOf(), mInput.GetAddressOf(), inputElementDesc, ARRAYSIZE(inputElementDesc));
	create_ps_from_cso(device, "Data/shader/static_mesh_ps.cso", mPSShader.GetAddressOf());
	//	//頂点シェーダー＆頂点入力レイアウトの生成
	//{
	//	// 頂点シェーダーファイルを開く(sprite_vs.hlsl をコンパイルしてできたファイル)
	//	FILE* fp = nullptr;
	//	fopen_s(&fp, "Data/shader/static_mesh_vs.cso", "rb");
	//	_ASSERT_EXPR_A(fp, "CSO File not found");

	//	// 頂点シェーダーファイルのサイズを求める
	//	fseek(fp, 0, SEEK_END);
	//	long cso_sz = ftell(fp);
	//	fseek(fp, 0, SEEK_SET);

	//	// メモリ上に頂点シェーダーデータを格納する領域を用意する
	//	std::unique_ptr<u_char[]> cso_data = std::make_unique<u_char[]>(cso_sz);
	//	fread(cso_data.get(), cso_sz, 1, fp);
	//	fclose(fp);

	//	// 頂点シェーダーデータを基に頂点シェーダーオブジェクトを生成する
	//	hr = device->CreateVertexShader(cso_data.get(), cso_sz, nullptr, mVSShader.GetAddressOf());
	//	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	//	// GPUに頂点データの内容を教えてあげるための設定
	//	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	//	{
	//	     { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	     { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	     { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	};

	//	// 入力レイアウトの生成
	//	hr = device->CreateInputLayout(
	//		input_element_desc,				// 頂点データの内容
	//		ARRAYSIZE(input_element_desc),	// 頂点データの要素数
	//		cso_data.get(),						// 頂点シェーダーデータ（input_element_descの内容と sprite_vs.hlslの内容に不一致がないかチェックするため）
	//		cso_sz,							// 頂点シェーダーデータサイズ
	//		mInput.GetAddressOf()					// 入力レイアウトオブジェクトのポインタの格納先。
	//	);
	//	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	//}

	//// ピクセルシェーダーの生成
	//{
	//	// ピクセルシェーダーファイルを開く(sprite_ps.hlsl をコンパイルしてできたファイル)
	//	FILE* fp = nullptr;
	//	fopen_s(&fp, "Data/shader/static_mesh_ps.cso", "rb");
	//	_ASSERT_EXPR_A(fp, "CSO File not found");

	//	// ピクセルシェーダーファイルのサイズを求める
	//	fseek(fp, 0, SEEK_END);
	//	long cso_sz = ftell(fp);
	//	fseek(fp, 0, SEEK_SET);

	//	// メモリ上に頂点シェーダーデータを格納する領域を用意する
	//	std::unique_ptr<u_char[]> cso_data = std::make_unique<u_char[]>(cso_sz);
	//	fread(cso_data.get(), cso_sz, 1, fp);
	//	fclose(fp);

	//	// ピクセルシェーダーの生成
	//	hr = device->CreatePixelShader(cso_data.get(), cso_sz, nullptr, mPSShader.GetAddressOf());
	//	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	//}

	// create rasterizer state : solid mode
	{
		D3D11_RASTERIZER_DESC rasterizerDesc = {};
		rasterizerDesc.FillMode = D3D11_FILL_SOLID; //D3D11_FILL_WIREFRAME, D3D11_FILL_SOLID
		rasterizerDesc.CullMode = D3D11_CULL_BACK; //D3D11_CULL_NONE, D3D11_CULL_FRONT, D3D11_CULL_BACK   
		rasterizerDesc.FrontCounterClockwise = FALSE;
		rasterizerDesc.DepthBias = 0;
		rasterizerDesc.DepthBiasClamp = 0;
		rasterizerDesc.SlopeScaledDepthBias = 0;
		rasterizerDesc.DepthClipEnable = false;
		rasterizerDesc.ScissorEnable = FALSE;
		rasterizerDesc.MultisampleEnable = FALSE;
		rasterizerDesc.AntialiasedLineEnable = FALSE;
		hr = device->CreateRasterizerState(&rasterizerDesc, mRasterizerState.GetAddressOf());
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	}
	// 深度ステンシルステートの設定
	{
		D3D11_DEPTH_STENCIL_DESC desc;
		::memset(&desc, 0, sizeof(desc));
		desc.DepthEnable = true;
		desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		desc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
		hr = device->CreateDepthStencilState(&desc, mDepthStencilState.GetAddressOf());
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	}
	// create constant buffer
	{
		D3D11_BUFFER_DESC bufferDesc = {};
		bufferDesc.ByteWidth = sizeof(CbScene);
		bufferDesc.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bufferDesc.CPUAccessFlags = 0;
		bufferDesc.MiscFlags = 0;
		bufferDesc.StructureByteStride = 0;
		hr = device->CreateBuffer(&bufferDesc, nullptr, mCbScene.GetAddressOf());
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

		bufferDesc.ByteWidth = sizeof(CbObj);

		hr = device->CreateBuffer(&bufferDesc, nullptr, mCbObj.GetAddressOf());
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	}

	// create sampler state
	D3D11_SAMPLER_DESC samplerDesc = {};
	samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0;
	samplerDesc.MaxAnisotropy = 16;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	memcpy(samplerDesc.BorderColor, &VECTOR4F(0.0f, 0.0f, 0.0f, 0.0f), sizeof(VECTOR4F));
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = device->CreateSamplerState(&samplerDesc, mSamplerState.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

}

void MeshRender::Begin(ID3D11DeviceContext* context, const VECTOR4F& light, const FLOAT4X4& view, const FLOAT4X4& projection)
{
	context->VSSetShader(mVSShader.Get(), nullptr, 0);
	context->PSSetShader(mPSShader.Get(), nullptr, 0);
	context->IASetInputLayout(mInput.Get());

	ID3D11Buffer* constant_buffers[] =
	{
		mCbScene.Get(),
		mCbObj.Get(),
	};
	context->VSSetConstantBuffers(0, ARRAYSIZE(constant_buffers), constant_buffers);
	context->PSSetConstantBuffers(0, ARRAYSIZE(constant_buffers), constant_buffers);

	context->OMSetDepthStencilState(mDepthStencilState.Get(), 0);
	context->PSSetSamplers(0, 1, mSamplerState.GetAddressOf());
	context->RSSetState(mRasterizerState.Get());

	
	cbScene.lightDirection = light;
	cbScene.view = view;
	cbScene.projection = projection;
	context->UpdateSubresource(mCbScene.Get(), 0, 0, &cbScene, 0, 0);

}

void MeshRender::Render(ID3D11DeviceContext* context, StaticMesh* obj, const FLOAT4X4& world, const VECTOR4F&color)
{
	FLOAT4X4 wvp;
	DirectX::XMStoreFloat4x4(&wvp, DirectX::XMLoadFloat4x4(&world) * DirectX::XMLoadFloat4x4(&cbScene.view) * DirectX::XMLoadFloat4x4(&cbScene.projection));

	for (StaticMesh::Mesh& mesh : obj->meshes)
	{
		u_int stride = sizeof(StaticMesh::Vertex);
		u_int offset = 0;
		context->IASetVertexBuffers(0, 1, mesh.vertexBuffer.GetAddressOf(), &stride, &offset);
		context->IASetIndexBuffer(mesh.indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		CbObj data;

		DirectX::XMStoreFloat4x4(&data.wvp, DirectX::XMLoadFloat4x4(&mesh.globalTransform) * DirectX::XMLoadFloat4x4(&wvp));
		DirectX::XMStoreFloat4x4(&data.world, DirectX::XMLoadFloat4x4(&mesh.globalTransform) * DirectX::XMLoadFloat4x4(&world));

		for (StaticMesh::Subset& subset : mesh.subsets)
		{
			data.color.x = subset.diffuse.color.x * color.x;
			data.color.y = subset.diffuse.color.y * color.y;
			data.color.z = subset.diffuse.color.z * color.z;
			data.color.w = color.w;
			context->UpdateSubresource(mCbObj.Get(), 0, 0, &data, 0, 0);

			context->PSSetShaderResources(0, 1, subset.diffuse.mShaderResourceView.GetAddressOf());
			context->DrawIndexed(subset.indexCount, subset.indexStart, 0);
		}
	}

}

void MeshRender::End(ID3D11DeviceContext* context)
{
}
