#include "bubble.h"
#include "bubble_manager.h"

Bubble::Bubble(std::shared_ptr<GeometricPrimitive> geo)
{
    bubble = std::make_shared<BubbleObj>(geo);
    bubble->SetScale(VECTOR3F(10, 10, 10));
    bubble->SetExist(false);
	bubble->SetHitObjt(false);
    pBubbleManager->SetBubbles(bubble);
	end = false;
}

Bubble::Bubble(ID3D11Device* device)
{
    bubble = std::make_shared<BubbleObj>(device);
	bubble->SetScale(VECTOR3F(10, 10, 10));
	bubble->SetExist(false);
    pBubbleManager->SetBubbles(bubble);
	end = false;
}

void Bubble::Update(float elapsed_time)
{
	VECTOR4F color = bubble->GetColor();
	if (end)
	{
		color.w -= elapsed_time;
		if (color.w <= 0.f)bubble->SetExist(false);

		if (bubble->GetHitObj())
		{
			end = false;
			color = VECTOR4F(1, 1, 1, 0.3f);
		}
	}
	else
	{
		if (!bubble->GetHitObj())end = true;
	}
	bubble->SetColor(color);
    bubble->Update();
}

void Bubble::Set(VECTOR3F pos, VECTOR3F angle)
{
	VECTOR3F scale = bubble->GetScale();
    bubble->SetPosition(VECTOR3F(pos.x + sinf(angle.y) * scale.x, pos.y+4.f, pos.z + cosf(angle.y) * scale.z));
    bubble->SetExist(true);
}
