#pragma once
#include"obj3d.h"
#include<memory>
#include"static_mesh.h"

class StaticObj :public Obj3D
{
public:
	StaticObj(std::shared_ptr<StaticMesh>mesh);
	int RayPick(const VECTOR3F& startPosition, const VECTOR3F& endPosition, VECTOR3F* outPosition, VECTOR3F* outNormal);
	int RayPick(const VECTOR3F& startPosition, const VECTOR3F& endPosition, VECTOR3F* outPosition, VECTOR3F* outNormal,float* outDistance);
	//getter
	const VECTOR4F& GetColor() { return mColor; }
	const VECTOR3F& GetHitArea() { return mHitArea; }
	const VECTOR3F& GetMin() { return mMin; }
	const VECTOR3F& GetMax() { return mMax; }
	StaticMesh* GetMesh() { return mMesh.get(); }
	const bool GetExist() { return exist; }
	//setter
	void SetColor(const VECTOR4F& color) { mColor = color; }
	void SetHitArea(const VECTOR3F& hitArea) { mHitArea = hitArea; }
	void SetMin(const VECTOR3F& min) { mMin = min; }
	void SetMax(const VECTOR3F& max) { mMax = max; }
	void SetExist(bool e) { exist = e; }
private:
	std::shared_ptr<StaticMesh>mMesh;
	VECTOR4F mColor;
	VECTOR3F mHitArea;
	VECTOR3F mMin;
	VECTOR3F mMax;
	bool exist;
};
