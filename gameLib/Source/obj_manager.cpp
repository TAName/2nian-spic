#include "obj_manager.h"
#include"camera_manager.h"
#include"collition.h"
#include"scene_manager.h"
#include"bubble_manager.h"
#include"framework.h"
#include"sound_manager.h"
#include"fado_out.h"

//#include<algorithm>
ObjManager* ObjManager::objManager = nullptr;
ObjManager::ObjManager(ID3D11Device*device)
{
	mMeshRender = std::make_unique<MeshRender>(device);
	mModelRender = std::make_unique<ModelRenderer>(device);
	mPrimitiveRender = std::make_unique<PrimitiveRender>(device);
	playerShadow = std::make_unique<Board>(device, L"Data/image/siro.png");
}
void ObjManager::HitPosition(VECTOR3F* playerPosition, const VECTOR3F& stageMin, const VECTOR3F& stageMax,const VECTOR3F&stagePosition,const VECTOR3F&playerLength)
{
	bool x = false, z = false;

	if (stagePosition.z + stageMin.z<=player->GetPosition().z && stagePosition.z + stageMax.z>=player->GetPosition().z)
	{
		z = true;
	}
	else if (stagePosition.x + stageMin.x<=player->GetPosition().x && stagePosition.x + stageMax.x>=player->GetPosition().x)
	{
		x = true;
	}

	if (x && z)
	{
		if (stagePosition.y + stageMin.y <= player->GetPosition().y)
		{
			playerPosition->y = stagePosition.y - stageMin.y - playerLength.y;
		}
		else if (stagePosition.y + stageMax.y >= player->GetPosition().y)
		{
			playerPosition->y = stagePosition.y - stageMax.y;
		}

	}
	else if (z && !x)
	{
		if (stagePosition.x + stageMin.x <= player->GetPosition().x)
		{
			playerPosition->x = stagePosition.x - stageMin.x + playerLength.x;
		}
		else if (stagePosition.x + stageMax.x >= player->GetPosition().x)
		{
			playerPosition->x = stagePosition.x - stageMax.x - playerLength.x;
		}
	}
	else if (!z && x)
	{
		if (stagePosition.z + stageMin.z <= player->GetPosition().z)
		{
			playerPosition->z = stagePosition.z - stageMin.z + playerLength.z;
		}
		else if (stagePosition.z + stageMax.z >= player->GetPosition().z)
		{
			playerPosition->z = stagePosition.z - stageMax.z - playerLength.z;
		}

	}
}
//bool AABB(const VECTOR3F& min1, const VECTOR3F& max1, const VECTOR3F& min2, const VECTOR3F& max2)
//{
//	return false;
//}
bool ObjManager::Judge(float elapsed_time)
{
#if USE_IMGUI
	auto remove01 = std::remove_if(stages.begin(), stages.end(), [](std::shared_ptr<StageObj>obj) {if (obj->GetExist())return false;else return true;});
	stages.erase(remove01, stages.end());
	auto remove02 = std::remove_if(buttons.begin(), buttons.end(), [](std::shared_ptr<ButtonObj>obj) {if (obj->GetExist())return false;else return true;});
	buttons.erase(remove02, buttons.end());
	auto remove03 = std::remove_if(gates.begin(), gates.end(), [](std::shared_ptr<GateObj>obj) {if (obj->GetExist())return false;else return true;});
	gates.erase(remove03, gates.end());
	auto remove04 = std::remove_if(moves.begin(), moves.end(), [](std::shared_ptr<MoveObj>obj) {if (obj->GetExist())return false;else return true;});
	moves.erase(remove04, moves.end());
#endif
	if (pFadoOut.GetFado())return true;
	if (player->GetO2() <= 0.f)
	{
		pSoundManager.Stop(SoundManager::SOUND::BGM_Game);

		pFadoOut.SetNextScene(SCENETYPE::OVER);

		return true;
	}
	VECTOR3F start = player->GetPosition();
	if (start.z <= -11.8f)
	{
		start.z = -11.8f;
		player->SetPosition(start);
	}
	VECTOR3F position,normal,end;
	float shadowLength = 0;
	end = start;
	start.y += 10;
	float angle_y = player->GetAngle().y;
	VECTOR3F front = VECTOR3F(sinf(angle_y), 0, cosf(angle_y));
	float playerLength = 1.0f;
	for (auto& stage : stages)
	{
		VECTOR3F stagePosition = stage->GetPosition();
		VECTOR3F stageMin = stage->GetMin();
		VECTOR3F stageMax = stage->GetMax();
		if (player->GetMoveFlag())
		{
			playerLength = 4.0f;
		}
		else
		{
			playerLength = 1.0f;

		}
		switch (stage->GetMeshNo())
		{
		case 0://床
			if (stage->RayPick(start, end, &position, &normal) != -1)
			{
				player->SetPosition(position);
				VECTOR3F savePosition = position;
				if (player->GetMoveFlag())
				{
					VECTOR3F angle = stage->GetAngle();
					VECTOR3F pAngle = player->GetAngle();

					float stageFront = cosf(angle.y);
					if (front.z != 0)
					{
						if (stageFront > 0)
						{
							if (front.x > 0)player->SetAngle(VECTOR3F(pAngle.x - angle.z, pAngle.y, pAngle.z));
							else if (front.x < 0)player->SetAngle(VECTOR3F(pAngle.x + angle.z, pAngle.y, pAngle.z));
						}
						else
						{
							if (front.x < 0)player->SetAngle(VECTOR3F(pAngle.x - angle.z, pAngle.y, pAngle.z));
							else if (front.x > 0)player->SetAngle(VECTOR3F(pAngle.x + angle.z, pAngle.y, pAngle.z));
						}
					}
					float angle_y = player->GetAngle().y;

					position -= VECTOR3F(sinf(angle_y) * 4.f, -2, cosf(angle_y) * 4.f);
					player->SetPosition(position);
				}
				player->CalculateMultipleTransform();
				player->SetPosition(savePosition);
			}
			for (auto& move : moves)
			{

				if (move->GetMoveFlage())
				{
					VECTOR3F movePosition = move->GetPosition();
					VECTOR3F moveMin = move->GetMin();
					VECTOR3F moveMax = move->GetMax();

					if (stage->RayPick(movePosition, movePosition+VECTOR3F(0,moveMax.y,0),&position,&normal) != -1)
					{
						move->SetPosition(position + VECTOR3F(0, -moveMax.y, 0));
						move->SetHitButton(true);
					}
				}
			}
			break;
		case 1://壁
				if (Collition::AABB(stagePosition + stageMin, stagePosition + stageMax, player->GetPosition() + VECTOR3F(-playerLength, 0, -playerLength), player->GetPosition() + VECTOR3F(playerLength, 2, playerLength)))
				{
					VECTOR3F newPosition = player->GetPosition();

					HitPosition(&newPosition, stageMin, stageMax, stagePosition, VECTOR3F(playerLength, 9, playerLength));
					player->SetPosition(newPosition);
				}


			break;
		case 2://ボタンの外枠
				if (Collition::AABB(stagePosition + stageMin, stagePosition + stageMax, player->GetPosition() + VECTOR3F(-playerLength, 0, -playerLength), player->GetPosition() + VECTOR3F(playerLength, 2, playerLength)))
				{
					VECTOR3F newPosition = player->GetPosition();

					HitPosition(&newPosition, stageMin, stageMax, stagePosition, VECTOR3F(playerLength, 9, playerLength));
					player->SetPosition(newPosition);
				}

			break;
		case 4://ゲートの外枠

			if (stage->RayPick(end, end + front * playerLength, &position, &normal) != -1)
			{
				position -= front * playerLength;
				player->SetPosition(position);
			}
			break;
		case 7:
			for (auto& move : moves)
			{

				if (move->GetMoveFlage())
				{
					VECTOR3F movePosition = move->GetPosition();
					VECTOR3F moveMin = move->GetMin();
					VECTOR3F moveMax = move->GetMax();

					if (stage->RayPick(movePosition, movePosition + VECTOR3F(0, moveMax.y, 0), &position, &normal) != -1)
					{
						move->SetPosition(position + VECTOR3F(0, -moveMax.y, 0));
						move->SetHitButton(true);
					}
				}
			}
			break;
		}
		if (stage->RayPick(start, end, &position, &normal) != -1)
		{
			if (shadowLength * shadowLength < (position.y - end.y) * (position.y - end.y))
			{
				shadowLength = position.y - end.y;
			}
		}
	}
	pShadowPosition = end;
	pShadowPosition.y += shadowLength+0.1f;

	for (auto& gate : gates)
	{
		VECTOR3F gatePosition = gate->GetPosition();
		VECTOR3F gateMin = gate->GetMin();
		VECTOR3F gateMax = gate->GetMax();
		if (gate->GetOpenFlag() && gate->GetOpenTime() >= 1.0f)gatePosition = gate->GetFramePosition();
		//if (Collition::AABB(gatePosition + gateMin, gatePosition + gateMax, player->GetPosition() + VECTOR3F(-playerLength, 0, -playerLength), player->GetPosition() + VECTOR3F(playerLength, 9, playerLength)))
		//{
		//	if (gate->GetOpenFlag() && gate->GetOpenTime() >= 1.0f)
		//	{
		//		pSceneManager.ChangeScene(SCENETYPE::CLEAR);
		//	}
		//	else
		//	{
		//		VECTOR3F newPosition = player->GetPosition();

		//		HitPosition(&newPosition, gateMin, gateMax, gatePosition, VECTOR3F(playerLength, 9, playerLength));
		//		player->SetPosition(newPosition);
		//	}
		//}
		if (player->GetMoveFlag())
		{
			playerLength = 4.f;
			if (Collition::AABB(gatePosition + gateMin, gatePosition + gateMax, player->GetPosition() + VECTOR3F(-playerLength, 0, -playerLength), player->GetPosition() + VECTOR3F(playerLength, 2, playerLength)))
			{
					if (gate->GetOpenFlag() && gate->GetOpenTime() >= 1.0f)
					{
						if (gates.size() - 1 == gate->GetGateNo())
						{
							pSoundManager.Stop(SoundManager::SOUND::BGM_Game);

							pFadoOut.SetNextScene(SCENETYPE::CLEAR);
							return true;;

						}
					}
					else
					{
						VECTOR3F newPosition = player->GetPosition();

						HitPosition(&newPosition, gateMin, gateMax, gatePosition, VECTOR3F(playerLength, 9, playerLength));
						player->SetPosition(newPosition);
					}
			}


		}
		else
		{
			playerLength = 1.0f;
			if (Collition::AABB(gatePosition + gateMin, gatePosition + gateMax, player->GetPosition() + VECTOR3F(-playerLength, 0, -playerLength), player->GetPosition() + VECTOR3F(playerLength, 2, playerLength)))
			{
					if (gate->GetOpenFlag() && gate->GetOpenTime() >= 1.0f)
					{
						pSoundManager.Stop(SoundManager::SOUND::BGM_Game);

						pFadoOut.SetNextScene(SCENETYPE::CLEAR);
						return true;;
					}
					else
					{
						VECTOR3F newPosition = player->GetPosition();

						HitPosition(&newPosition, gateMin, gateMax, gatePosition, VECTOR3F(playerLength, 9, playerLength));
						player->SetPosition(newPosition);
					}
			}


		}

	}
	//泡で浮かせれるオブジェクト
	for (auto& move : moves)
	{
		VECTOR3F movePosition = move->GetPosition();
		VECTOR3F moveMin = move->GetMin();
		VECTOR3F moveMax = move->GetMax();

		if (Collition::AABB(movePosition + moveMin, movePosition + moveMax, player->GetPosition() + VECTOR3F(-playerLength, 0, -playerLength), player->GetPosition() + VECTOR3F(playerLength, 9, playerLength)))
		{
			VECTOR3F newPosition = player->GetPosition();

			HitPosition(&newPosition, moveMin, moveMax, movePosition, VECTOR3F(playerLength, 9, playerLength));
			player->SetPosition(newPosition);
		}
        if (!move->GetMoveFlage()) {

            for (auto& bubble : pBubbleManager->GetBubble()) {
				if (bubble->GetHitObj())continue;
                DirectX::XMVECTOR mpos = DirectX::XMLoadFloat3(&movePosition);
                DirectX::XMVECTOR bpos = DirectX::XMLoadFloat3(&bubble->GetPosition());
                float len;
                DirectX::XMStoreFloat(&len, DirectX::XMVector3Length(DirectX::XMVectorSubtract(mpos, bpos)));
                if (len < moveMax.x + bubble->GetScale().x) {
                    move->SetMoveFlag(true);
                    move->SetBubble(bubble);
					bubble->SetHitObjt(true);

                }
            }
        }

		for (auto& button : buttons)
		{
			VECTOR3F buttonPosition = button->GetPosition();
			VECTOR3F buttonMin = button->GetMin();
			VECTOR3F buttonMax = button->GetMax();
			if (!button->GetPushFlag())
			{
				if (Collition::AABB(movePosition + moveMin, movePosition + moveMax, buttonPosition + buttonMin, buttonPosition + buttonMax))
				{
					VECTOR3F newPosition = movePosition;

					HitPosition(&newPosition, buttonMax, buttonMin, buttonPosition, moveMax);
					move->SetPosition(newPosition);
					button->SetPushFlag(true);
					move->SetHitButton(true);
				}
			}
		}

	}

	for (auto& supply : supplys)
	{
		DirectX::XMVECTOR mpos = DirectX::XMLoadFloat3(&player->GetPosition());
		DirectX::XMVECTOR bpos = DirectX::XMLoadFloat3(&supply->GetPosition());
		float len;
		DirectX::XMStoreFloat(&len, DirectX::XMVector3Length(DirectX::XMVectorSubtract(mpos, bpos)));
		if (len < supply->GetHitLength() + playerLength)
		{
			float o2 = player->GetO2();

			o2 += 2.0f;
			if (o2 > 1000)
			{
				o2 = 1000;
			}
			else
			{
				supply->SetSupplyFlag(true);
			}
			player->SetO2(o2);
		}
	}
#ifdef USE_IMGUI
	ImGui::Begin("chack");
	ImGui::Text("a.x %f:a.y %f:a.z %f", player->GetAngle().x, player->GetAngle().y, player->GetAngle().z);
	ImGui::Text("o2 %f", player->GetO2());
	ImGui::End();
#endif
	return false;
}

void ObjManager::Render(ID3D11DeviceContext* context, const VECTOR4F& light, const FLOAT4X4& view, const FLOAT4X4& projection)
{

	DirectX::XMVECTOR eye = DirectX::XMLoadFloat3(&pCamera.GetCamera()->GetEye());
	DirectX::XMVECTOR front = DirectX::XMLoadFloat3(&pCamera.GetCamera()->GetFront());
	float dot;

	mMeshRender->Begin(context, light, view, projection);
	for (auto& stage : stages)
	{
		DirectX::XMVECTOR position = DirectX::XMLoadFloat3(&stage->GetPosition());
		DirectX::XMStoreFloat(&dot, DirectX::XMVector3Dot(front, DirectX::XMVector3Normalize(DirectX::XMVectorSubtract(position, eye))));
		if (dot <= 0.8f)continue;
		mMeshRender->Render(context, stage->GetMesh(), stage->GetWorld());
	}
	for (auto& arrow : arrows)
	{
		mMeshRender->Render(context, arrow->GetMesh(), arrow->GetWorld(), arrow->GetColor());
	}
	for (auto& button : buttons)
	{
		DirectX::XMVECTOR position = DirectX::XMLoadFloat3(&button->GetPosition());
		DirectX::XMStoreFloat(&dot, DirectX::XMVector3Dot(front, DirectX::XMVector3Normalize(DirectX::XMVectorSubtract(position, eye))));
		if (dot <= 0.8f)continue;

		mMeshRender->Render(context, button->GetMesh(), button->GetWorld(), button->GetColor());
	}
	for (auto& gate : gates)
	{
		DirectX::XMVECTOR position = DirectX::XMLoadFloat3(&gate->GetPosition());
		DirectX::XMStoreFloat(&dot, DirectX::XMVector3Dot(front, DirectX::XMVector3Normalize(DirectX::XMVectorSubtract(position, eye))));
		if (dot <= 0.8f)continue;

		mMeshRender->Render(context, gate->GetMesh(), gate->GetWorld(), gate->GetColor());
	}
	for (auto& move : moves)
	{
		DirectX::XMVECTOR position = DirectX::XMLoadFloat3(&move->GetPosition());
		DirectX::XMStoreFloat(&dot, DirectX::XMVector3Dot(front, DirectX::XMVector3Normalize(DirectX::XMVectorSubtract(position, eye))));
		if (dot <= 0.8f)continue;

		mMeshRender->Render(context, move->GetMesh(), move->GetWorld());
	}
	if(debugHitArea!=nullptr)mMeshRender->Render(context, debugHitArea->GetMesh(), debugHitArea->GetWorld(),VECTOR4F(1,1,1,0.5f));

	mMeshRender->End(context);
	mPrimitiveRender->Begin(context, light, view, projection);
	for (auto& supply : supplys)
	{
		mPrimitiveRender->Render(context, supply->GetPrimitive().get(), supply->GetWorld(), VECTOR4F(1, 1, 1, 0.2f));
	}
	mPrimitiveRender->End(context);

	FLOAT4X4 viewProjection;
		FLOAT4X4 view_projection;
	DirectX::XMMATRIX C = DirectX::XMMatrixSet(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);

	DirectX::XMStoreFloat4x4(&view_projection, C * DirectX::XMLoadFloat4x4(&view) * DirectX::XMLoadFloat4x4(&projection));
	playerShadow->Render(context, pShadowPosition, VECTOR3F(2, 2, 2), VECTOR3F(DirectX::XMConvertToRadians(90.0f), 0, 0), view, projection, VECTOR4F(0, 0, 0, 0.5f));
	mModelRender->Begin(context, view_projection, light);
	mModelRender->Draw(context, *player->GetModel());
	mModelRender->End(context);


}

ObjManager::~ObjManager()
{
	mMeshRender.reset();
	mModelRender.reset();
	player.reset();
	stages.clear();
}
