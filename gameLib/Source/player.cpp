#include "player.h"
#include "obj_manager.h"
#include"key_board.h"
#include"bubble_manager.h"

Player::Player(std::shared_ptr<ModelResource>resource)
{
    PL = std::make_shared<PlayerObj>(resource);
    pObjManager->SetPlayer(PL);
    PL->SetScale(VECTOR3F(1, 1, 1));
    PL->SetAnim(1);
    O2 = 1000;
	PL->SetO2(O2);
	PL->SetMoveFlaf(false);
}

void Player::Update(float elapsedTime)
{
	O2 = PL->GetO2();
    PL->CalculateMultipleTransform();
    Move(elapsedTime);
    VECTOR3F position = PL->GetPosition();
    VECTOR3F savePosition = position;
	if (PL->GetMoveFlag())
	{
		float angle_y = PL->GetAngle().y;

		position -= VECTOR3F(sinf(angle_y) * 4.f, -2, cosf(angle_y) * 4.f);
		PL->SetPosition(position);
	}
    PL->AnimUpdate(elapsedTime);
    PL->CalculateMultipleTransform();
    PL->SetPosition(savePosition);
}

void Player::Move(float elapsedTime)
{
    const float speed = 10.0f;
    pos = PL->GetPosition();
    angle = PL->GetAngle();
    angle.z = DirectX::XMConvertToRadians(0);
	bool move = false;
	
    if (pKeyBoad.PressedState(KeyLabel::RIGHT)) {
        pos.x += speed * elapsedTime;
        angle.y = DirectX::XMConvertToRadians(90);
        ASet(true);
		move = true;
    }
    if (pKeyBoad.PressedState(KeyLabel::LEFT)) {
        pos.x -= speed * elapsedTime;
        angle.y = DirectX::XMConvertToRadians(-90);
        ASet(true);
		move = true;

    }
    if (pKeyBoad.PressedState(KeyLabel::UP)) {
        pos.z += speed * elapsedTime;
        angle.y = DirectX::XMConvertToRadians(0);
        ASet(true);
		move = true;

    }
    if (pKeyBoad.PressedState(KeyLabel::DOWN)) {
        pos.z -= speed * elapsedTime;
        angle.y = DirectX::XMConvertToRadians(180);
        ASet(true);
		move = true;

    }
    if(!pKeyBoad.PressedState(KeyLabel::RIGHT)&&! pKeyBoad.PressedState(KeyLabel::LEFT)&&! pKeyBoad.PressedState(KeyLabel::UP)&&! pKeyBoad.PressedState(KeyLabel::DOWN)) {
        ASet(false);
    }
	if (pKeyBoad.RisingState(KeyLabel::Z))
	{
        BubbleSet();
	}
    //�_�f�c�ʌ���
    if (O2 <= 0.f) 
        O2 = 0.f;
    else 
        O2 -=0.05f;
	PL->SetO2(O2);
    pos.y -= 0.3f;
    PL->SetPosition(pos);
    PL->SetAngle(angle);
	PL->SetMoveFlaf(move);
}

void Player::BubbleSet()
{
    if (O2 > 35.f) {
        pBubbleManager->AA();
        pBubbleManager->Set(pos, angle);
        O2 -= 35.f;
    }
}
void Player::ASet(bool flag) {
    if (flag) {
        a++;
        angle.x = DirectX::XMConvertToRadians(90);
        if (a == 1) {
            PL->SetAnim(2);
            b = 0;
        }
    }
    if(!flag) {
        b++;
        angle.x = DirectX::XMConvertToRadians(0);
        if (b == 1) {
            PL->SetAnim(1);
            a = 0;
        }
    }
}