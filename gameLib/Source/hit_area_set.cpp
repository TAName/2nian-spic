#include "hit_area_set.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif
#include"obj_manager.h"

HitAreaSet::HitAreaSet(ID3D11Device* device, const int meshNo):operationFlag(false),mMeshNo(0)
{
	std::shared_ptr<StaticMesh>mesh = std::make_shared<StaticMesh>(device, "Data/FBX/000_cube/000_cube.fbx");
	setArea.resize(meshNo);
	drowObj = std::make_shared<StaticObj>(mesh);
	pObjManager->SetDebugHitArea(drowObj);
	drowObj->CalculateTransform();

}

void HitAreaSet::SetDragObj(std::shared_ptr<StaticObj> obj, const int meshNo)
{
	setObj = obj;
	operationFlag = true;
	VECTOR3F hitArea = setObj->GetHitArea();
	area[0] = hitArea.x;
	area[1] = hitArea.y*0.5f;
	area[2] = hitArea.z;
	mMeshNo = meshNo;

}

bool HitAreaSet::Editor()
{
	if (!operationFlag)return operationFlag;
#ifdef USE_IMGUI
	ImGui::Begin("hitAreaSet");
	if (ImGui::DragFloat3("hitArea", area, 0.1f, 0.0f, 1000.0f))
	{
		setArea[mMeshNo] = VECTOR3F(area[0], area[1] * 2.0f, area[2]);
		setObj->SetHitArea(setArea[mMeshNo]);
	}
	if (ImGui::Button("end"))
	{
		operationFlag = false;
		setObj.reset();
	}
	ImGui::End();
#endif
	if (setObj != nullptr)
	{
		drowObj->SetPosition(VECTOR3F(setObj->GetPosition().x, setObj->GetPosition().y + area[1], setObj->GetPosition().z));
		drowObj->SetScale(VECTOR3F(area[0], area[1], area[2]));
		drowObj->CalculateTransform();
	}
	else
	{
		drowObj->SetScale(VECTOR3F(0, 0, 0));
		drowObj->CalculateTransform();
	}
	return operationFlag;
}
