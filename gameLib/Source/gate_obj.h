#pragma once
#include"static_obj.h"

class GateObj :public StaticObj
{
public:
	GateObj(std::shared_ptr<StaticMesh>mesh, int gateNo);
	//setter
	void SetOpenFlag(const bool open) { openFlag = open; }
	void SetOpenTime(const float time) { openTime = time; }
	void SetFramePosition(const VECTOR3F& position) { framePosition = position; }

	//getter
	const bool GetOpenFlag() { return openFlag; }
	const int GetGateNo() { return mGateNo; }
	const float GetOpenTime() { return openTime; }
	const VECTOR3F& GetFramePosition() { return framePosition; }

private:
	int mGateNo;
	int openFlag;
	float openTime;
	VECTOR3F framePosition;
};