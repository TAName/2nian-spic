#include "drag_operation.h"
#include"obj_manager.h"
#include"key_board.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif
#include"key_board.h"
#include"camera_manager.h"

DragOperation::DragOperation(ID3D11Device* device) :dragFlag(false), operationFlag(false), hitDragArrow(-1), defaultPosition(0, 0, 0), beforeMousePosition(0, 0, 0), defaultOutPosition(0, 0, 0)
{
	std::shared_ptr<StaticMesh>mesh;
	mesh = std::make_shared<StaticMesh>(device, "Data/FBX/Arrow/arrow.fbx");
	for (int i = 0;i < 3;i++)
	{
		arrow[i] = std::make_shared<StaticObj>(mesh);
		arrow[i]->SetScale(VECTOR3F(0, 0, 0));
		arrow[i]->SetColor(VECTOR4F(1, 1, 1, 0.7f));
	}
	loacalPosition[0] = VECTOR3F(15, 1, 0);
	loacalPosition[1] = VECTOR3F(0, 16, 0);
	loacalPosition[2] = VECTOR3F(0, 1, -15);

	arrow[0]->SetAngle(VECTOR3F(0, 0, DirectX::XMConvertToRadians(90.f)));
	arrow[1]->SetAngle(VECTOR3F(0, 0, DirectX::XMConvertToRadians(180.f)));
	arrow[2]->SetAngle(VECTOR3F(DirectX::XMConvertToRadians(90.f), 0, 0));
	for (int i = 0;i < 3;i++)
	{
		arrow[i]->SetPosition(defaultPosition + loacalPosition[i]);
		arrow[i]->CalculateTransform();
		pObjManager->SetArrows(arrow[i]);
	}
}

void DragOperation::SetDragObj(std::shared_ptr<StaticObj>obj)
{
	dragObj = obj;
	operationFlag = true;
	defaultPosition = dragObj->GetPosition();
	VECTOR3F scale = dragObj->GetScale();
	for (int i = 0;i < 3;i++)
	{
		arrow[i]->SetPosition(defaultPosition);
		arrow[i]->SetScale(VECTOR3F(0.025f* scale.x, 0.05f* scale.y, 0.025f* scale.z));
		arrow[i]->CalculateTransform();
	}
}

bool DragOperation::Editor(const VECTOR3F& start, const VECTOR3F& end)
{
	VECTOR3F outPosition, outNormal;
	if (!operationFlag)
	{
		if (dragObj != nullptr)
		{
			float scale = dragObj->GetScale().x;
			for (int i = 0;i < 3;i++)
			{
				arrow[i]->SetScale(VECTOR3F(0, 0, 0));
				arrow[i]->SetPosition(defaultPosition + loacalPosition[i] * scale);
				arrow[i]->CalculateTransform();

			}
			dragObj.reset();


		}
		return false;
	}
#ifdef USE_IMGUI
	if (!dragFlag)
	{
		for (int i = 0;i < 3;i++)
		{
			if (arrow[i]->RayPick(start, end, &outPosition, &outNormal) != -1)
			{
				defaultOutPosition = outPosition;
				hitDragArrow = i;
				arrow[i]->SetColor(VECTOR4F(1, 1, 1, 1));
			}
			else
			{
				arrow[i]->SetColor(VECTOR4F(1, 1, 1, 0.7f));
			}
		}
	}
	float scale = dragObj->GetScale().x;

	ImGui::Begin("drag");
	float*dragPosition[] = { &defaultPosition.x,&defaultPosition.y,&defaultPosition.z };
	ImGui::DragFloat3("dragObj:position", dragPosition[0]);
	
	if (ImGui::DragFloat("dragObj:scale", &scale,0.1f,0.0f,1000.0f)/*&& scale>=0*/)
	{
		dragObj->SetScale(VECTOR3F(scale, scale, scale));
		for (int i = 0;i < 3;i++)
		{
			arrow[i]->SetScale(VECTOR3F(0.025f, 0.05f, 0.025f) *scale);
		}

	}
	VECTOR3F a=dragObj->GetAngle();
	a.x = DirectX::XMConvertToDegrees(a.x);
	a.y = DirectX::XMConvertToDegrees(a.y);
	a.z = DirectX::XMConvertToDegrees(a.z);
	float* dragAngle[] = { &a.x,&a.y,&a.z };
	if (ImGui::DragFloat3("dragObj:angle", dragAngle[0], 10, .0f, 360.0f))
	{
		a.x = DirectX::XMConvertToRadians(a.x);
		a.y = DirectX::XMConvertToRadians(a.y);
		a.z = DirectX::XMConvertToRadians(a.z);
		dragObj->SetAngle(a);
	}
	if (ImGui::Button("delete"))
	{
		dragObj->SetExist(false);
		dragObj.reset();
		operationFlag = false;
		ImGui::End();
		for (int i = 0;i < 3;i++)
		{
			arrow[i]->SetScale(VECTOR3F(0, 0, 0));
			arrow[i]->SetPosition(defaultPosition + loacalPosition[i] * scale);
			arrow[i]->CalculateTransform();

		}

		return true;
	}
	if (ImGui::Button("end"))
	{
		operationFlag = false;
		for (int i = 0;i < 3;i++)
		{
			arrow[i]->SetScale(VECTOR3F(0, 0, 0));
			arrow[i]->SetPosition(defaultPosition + loacalPosition[i] * scale);
			arrow[i]->CalculateTransform();

		}
		dragObj.reset();
		ImGui::End();
		return true;
	}

	ImGui::End();

	if (pKeyBoad.PressedState(VK_LBUTTON) && !pKeyBoad.PressedState(VK_MENU))
	{
		dragFlag = true;
		if (hitDragArrow != -1)Move(start, end);
	}
	else
	{
		dragFlag = false;
		hitDragArrow = -1;
	}
	beforeMousePosition = end;
	for (int i = 0;i < 3;i++)
	{
		arrow[i]->SetPosition(defaultPosition + loacalPosition[i]* scale);
		arrow[i]->CalculateTransform();
	}
	dragObj->SetPosition(defaultPosition);

#endif

	return true;
}

void DragOperation::Move(const VECTOR3F& start, const VECTOR3F& end)
{
	//VECTOR3F position = arrow[hitDragArrow]->GetPosition();
	float cameraFar = pCamera.GetCamera()->GetFar();
	DirectX::XMVECTOR defPosition = DirectX::XMLoadFloat3(&defaultOutPosition);
	DirectX::XMVECTOR eye = DirectX::XMLoadFloat3(&pCamera.GetCamera()->GetEye());
	DirectX::XMVECTOR length = DirectX::XMVector3Length(DirectX::XMVectorSubtract(defPosition, eye));
	float fLength;
	DirectX::XMStoreFloat(&fLength, length);
	VECTOR3F speed;
	switch (hitDragArrow)
	{
	case 0:
		speed.x = (end.x - beforeMousePosition.x) / cameraFar * fLength;
		break;
	case 1:
		speed.y += (end.y - beforeMousePosition.y) / cameraFar * fLength;
		break;
	case 2:
		speed.z += (end.z - beforeMousePosition.z) / cameraFar * fLength;
		break;
	}
	//position += speed;
	defaultPosition += speed;
	//arrow[hitDragArrow]->SetPosition(position);
	//position = defaultPosition;
}
