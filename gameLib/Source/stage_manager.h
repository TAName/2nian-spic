#pragma once
#include"stage_obj.h"
#include<vector>
#include"drag_operation.h"
#include"sprite.h"
#include"stage.h"
#include"stage_gate.h"
#include"stage_button.h"
#include"hit_area_set.h"
#include"background.h"
#include"stage_move.h"
#include"supply_area.h"

class StageManager
{
public:
	static void Create(ID3D11Device* device)
	{
		if (manager != nullptr)return;
		manager = new StageManager(device);
	}
	static void Destroy()
	{
		if (manager == nullptr)return;
		delete manager;
		manager = nullptr;
	}
	static StageManager* GetInctance()
	{
		return manager;
	}
	void Load(const int stageNo);
	void Save(const int stageNo);
	bool Editor();
	void Update(float elapasd_time);	
	void StageEditor();
	bool Select(float elapasd_time);
	bool SelectModo();
	bool StageSelect(float elapasd_time);
	bool SelectRender(ID3D11DeviceContext* context);
	~StageManager();
	//getter
	//settter
	void SetStageObj(std::shared_ptr<StageObj>obj) { mObjects.push_back(obj); }
private:
	struct SaveData
	{
		int meshesNo = 0;
		VECTOR3F position;
		VECTOR3F scale;
		VECTOR3F angle;
	};
	enum MODO
	{
		GAME,
		EDITOR
	};
	enum MESHTYPE
	{
		FLOOR,
		WALL,
		BUTTON,
		GORL= BUTTON+2,
		MOVE=GORL+2
	};
	int nowModo;
	static StageManager* manager;
	int meshNo;
	int mStageNo;
	bool selectFlag;
	bool editorFlag;
	bool modoSelectFlag;
	bool dragFlag;
	int dragNo;
	int gateNo;
	float selectMoveTimer;
	int mBeforeSelectNo;
	int objSetModo;
	std::vector<std::shared_ptr<StaticMesh>>mMeshes;
	std::vector<std::shared_ptr<Stage>>mStage;
	std::vector<std::shared_ptr<StageButton>>mButton;
	std::vector<std::shared_ptr<StageGate>>mGate;
	std::vector<std::shared_ptr<StageObj>>mObjects;
	std::vector<std::shared_ptr<BackGround>>mBackGrounds;
	std::vector<std::shared_ptr<BackGroundObj>>mBackObjs;
	std::vector<std::shared_ptr<StageMove>>mMoves;
	std::vector<std::shared_ptr<SupplyArea>>mAreas;
	std::unique_ptr<DragOperation>mArrow;
	std::unique_ptr<HitAreaSet>mHitArea;
	std::vector<std::string>stageName;
	std::vector<std::string>meshName;
	std::unique_ptr<Sprite>selectFrameSprite;
	std::unique_ptr<Sprite>selectNumberSprite;
	std::unique_ptr<Sprite>selectArrowSprite;
	std::shared_ptr<GeometricPrimitive>primitive;
	StageManager(ID3D11Device* device);
	void SetStage();
	void ScreeenToWorld(VECTOR3F* worldPosition, const VECTOR3F& screenPosition);
	void MouseToWorld();
	void CreateObj(int number,const VECTOR3F&position,const VECTOR3F& scale=VECTOR3F(1,1,1), const VECTOR3F& angle= VECTOR3F(0, 0, 0));
	void ResetModo()
	{
		modoSelectFlag = false;
		selectFlag = false;
		editorFlag = false;
	}
	VECTOR3F mNearPosition;
	VECTOR3F mFarPosition;
	VECTOR3F mStartCreatePosition;
};
#define pStageManager (StageManager::GetInctance())