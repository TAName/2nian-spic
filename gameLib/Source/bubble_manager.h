#pragma once
#include "geometric_primitive.h"
#include "bubble.h"
#include <memory>
#include<vector>
class BubbleManager {
public:
    static void Create(ID3D11Device* device) {
        if (bubblemanager != nullptr)return;
        bubblemanager = new BubbleManager(device);
    }
    static void Destroy()
    {
        if (bubblemanager == nullptr)return;
        delete bubblemanager;
        bubblemanager = nullptr;
    }
    void Updeta(float elapsed_time);
    void Render(ID3D11DeviceContext* context, const VECTOR4F& light, const FLOAT4X4& view, const FLOAT4X4& projection);
    void Set(VECTOR3F pos, VECTOR3F angle);
    static BubbleManager* GetInctancee() {
        return bubblemanager;
    }
    void AA();
    void SetBubbles(std::shared_ptr<BubbleObj>bubble) { bubbles.push_back(bubble); }
    std::vector<std::shared_ptr<BubbleObj>>GetBubble() {
        return bubbles;
    }
    ~BubbleManager();
private:
    BubbleManager(ID3D11Device* device);
    std::unique_ptr<PrimitiveRender> PriRender;
    std::vector<std::shared_ptr<BubbleObj>>bubbles;
    std::vector<std::shared_ptr<Bubble>> bub;
    std::shared_ptr<GeometricPrimitive> geome;
    static BubbleManager* bubblemanager;
};
#define pBubbleManager (BubbleManager::GetInctancee())