#pragma once
#include"player_obj.h"
#include"stage_obj.h"
#include"vector.h"
#include"model_renderer.h"
#include"gate_obj.h"
#include"button_obj.h"
#include"board.h"
#include"move_obj.h"
#include"supply_area_obj.h"

class ObjManager
{
public:
	static void Create(ID3D11Device* device)
	{
		if (objManager != nullptr)return;
		objManager = new ObjManager(device);
	}
	static void Destroy()
	{
		if (objManager == nullptr)return;
		delete objManager;
		objManager = nullptr;
	}
	static ObjManager* GetInctance()
	{
		return objManager;
	}
	bool Judge(float elapsed_time);
	void Render(ID3D11DeviceContext* context, const VECTOR4F& light, const FLOAT4X4& view, const FLOAT4X4& projection);
	//setter
	void SetPlayer(std::shared_ptr<PlayerObj>p) { player = p; }
	void SetStages(std::shared_ptr<StageObj>stage) { stages.push_back(stage); }
	void SetArrows(std::shared_ptr<StaticObj>arrow) { arrows.push_back(arrow); }
	void SetDebugHitArea(std::shared_ptr<StaticObj>hitArea) { debugHitArea = hitArea; }
	void SetGates(std::shared_ptr<GateObj>gate) { gates.push_back(gate); }
	void SetButtons(std::shared_ptr<ButtonObj>button) { buttons.push_back(button); }
	void SetMoves(std::shared_ptr<MoveObj>move) { moves.push_back(move); }
	void SetSupplyss(std::shared_ptr<SupplyAreaObj> supply) { supplys.push_back(supply); }
	//getter
	std::vector<std::shared_ptr<GateObj>>GetGateObj()
	{
		return gates;
	}
	void ClearStage() 
	{ 
		stages.clear(); 
		gates.clear();
		buttons.clear();
		moves.clear();
		supplys.clear();
	}
	~ObjManager();
private:
	ObjManager(ID3D11Device* device);
	void HitPosition(VECTOR3F* playerPosition, const VECTOR3F& stageMin, const VECTOR3F& stageMax, const VECTOR3F& stagePosition, const VECTOR3F& playerLength);
	static ObjManager* objManager;
	std::vector<std::shared_ptr<StageObj>>stages;
	std::vector<std::shared_ptr<GateObj>>gates;
	std::vector<std::shared_ptr<ButtonObj>>buttons;
	std::vector<std::shared_ptr<MoveObj>>moves;
	std::vector<std::shared_ptr<StaticObj>>arrows;
	std::vector<std::shared_ptr<SupplyAreaObj>>supplys;
	std::shared_ptr<PlayerObj>player;
	std::unique_ptr<MeshRender>mMeshRender;
	std::unique_ptr<ModelRenderer>mModelRender;
	std::unique_ptr<PrimitiveRender>mPrimitiveRender;
	std::unique_ptr<Board>playerShadow;
	std::shared_ptr<StaticObj>debugHitArea;
	VECTOR3F pShadowPosition;
	
};
#define pObjManager (ObjManager::GetInctance())