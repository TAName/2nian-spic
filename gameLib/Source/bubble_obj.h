#pragma once
#include "obj3d.h"
#include "geometric_primitive.h"
#include<memory>
class BubbleObj :public Obj3D
{
public:
    BubbleObj(std::shared_ptr<GeometricPrimitive>geo);
    BubbleObj(ID3D11Device* device);
    void Update();

    const VECTOR4F& GetColor() { return Color; }
	void const SetColor(const VECTOR4F& color) { Color = color; }
    GeometricPrimitive* GetSphere() { return Sphere.get(); }
    const bool GetExist() { return exist; }
    void SetExist(bool e) { exist = e; }
	const bool GetHitObj() { return hitObj; }
	void SetHitObjt(bool e) { hitObj = e; }

private:
    std::shared_ptr<GeometricPrimitive>Sphere;
    VECTOR4F Color = { 1,1,1,0.3f };
    bool exist;
	bool hitObj;
};